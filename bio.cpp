#include "bio.h"
#include "files.h"
#include <stdlib.h> // atoi

using namespace std;

const std::string Molecule::REVENZ = "REVENZ"; //!< Reversible Enzyme
const std::string Molecule::IRRENZ = "IRRENZ"; //!< Irreversible Enzyme
const std::string Molecule::INTMET = "INTMET"; //!< Internal Metabolite
const std::string Molecule::EXTMET = "EXTMET"; //!< External Metabolite
const std::string Molecule::METABOLITE = "METABOLITE"; //!< Metabolite
const std::string Molecule::REACTION = "REACTION"; //!< Reaction
const std::string Molecule::REACTANT = "REACTANT"; //!< Reactant
const std::string Molecule::PRODUCT = "PRODUCT"; //!< Product

std::map<std::string, const int> MoleculeTypes =
{
    {Molecule::REVENZ,1},
    {Molecule::IRRENZ,2},
    {Molecule::INTMET,3},
    {Molecule::EXTMET,4},
    {Molecule::METABOLITE,5},
    {Molecule::REACTION,6},
    {Molecule::PRODUCT,7},
    {Molecule::REACTANT,8}
};

/*
{
		std::make_pair(Molecule::REVENZ,1),
		std::make_pair(Molecule::IRRENZ,2),
		std::make_pair(Molecule::INTMET,3),
		std::make_pair(Molecule::EXTMET,4),
		std::make_pair(Molecule::METABOLITE,1),
		std::make_pair(Molecule::REACTION,2),
		std::make_pair(Molecule::REACTANT,1),
		std::make_pair(Molecule::PRODUCT,2)
};

const char * const Molecule::days[] = {"mon", "tue", "wed", "thur",
                                       "fri", "sat", "sun"};

const char* const MoleculeTypes[] = {"REVENZ", "IRRENZ", "INTMET", "EXTMET",
		"METABOLITE","REACTION","REACTANT","PRODUCT"};

*/
std::string Molecule::toStringType() {
	cout <<endl;
	return "";
}

int Molecule::convertType2String(std::string& typestring) {
	return 1;
}

Reaction::Reaction(std::string name, std::vector<Reactant> reactants, std::vector<Product> products) {
	_name = name;
	_reactants = reactants;
	_products = products;
}

Reaction::Reaction(Reaction &r) :
	_name(r._name),
	_reactants(r._reactants),
	_products(r._products)
{}

vector<Reactant> Reaction::getReactants() {
	return _reactants;
}

void Reaction::setReactants(vector<Reactant> reactants) {
	_reactants = reactants;
}

vector<Product> Reaction::getProducts() {
	return _products;
}

void Reaction::setProducts(vector<Product> products) {
	_products = products;
}

void Reaction::display() {
	cout << _name << " : ";
	for (unsigned i = 0; i < _reactants.size() - 1; i++)
		if (_reactants[i].getCoefficient() > 1)
			cout << _reactants[i].getCoefficient() << _reactants[i].getName()
			        << " + ";
		else
			cout << _reactants[i].getName() << " + ";
	if (_reactants[_reactants.size() - 1].getCoefficient() > 1)
		cout << _reactants[_reactants.size() - 1].getCoefficient()
		        << _reactants[_reactants.size() - 1].getName() << " = ";
	else
		cout << _reactants[_reactants.size() - 1].getName() << " = ";

	for (unsigned i = 0; i < _products.size() - 1; i++)
		if (_products[i].getCoefficient() > 1)
			cout << _products[i].getCoefficient() << _products[i].getName()
			        << " + ";
		else
			cout << _products[i].getName() << " + ";
	if (_products[_products.size() - 1].getCoefficient() > 1)
		cout << _products[_products.size() - 1].getCoefficient()
		        << _products[_products.size() - 1].getName() << endl;
	else
		cout << _products[_products.size() - 1].getName() << endl;
}

void Reaction::printEdges() {
	for (unsigned i = 0; i < _reactants.size(); i++)
		cout << _name << " " << _reactants[i].getName() << endl;
	for (unsigned i = 0; i < _products.size(); i++)
		cout << _name << " " << _products[i].getName() << endl;
}

void Reaction::createReaction(string reaction) {
	// Parse a reaction out of elements: reaction name, left side and right side
	size_t found_dc = reaction.find(":"); // Position of double colon
	size_t found_eq = reaction.find("="); // Position of equal

	// For each reaction name
	string r_name = reaction.substr(0, found_dc - 1);
	// Get the compounds on the left side of the reaction
	string r_reactants = reaction.substr(found_dc + 1, found_eq - found_dc - 1);
	// Get the compounds on the right side of the reaction
	string r_products = reaction.substr(found_eq + 1,
	        reaction.length() - found_eq - 2);

	vector<string> reactants = normalize_reaction(r_reactants);
	vector<string> products = normalize_reaction(r_products);

	_name = trim(r_name);

	for (unsigned i = 0; i < reactants.size(); i += 2) {
		Reactant sub;
		sub.setCoefficient(atoi(reactants[i].c_str()));
		sub.setName(reactants[i + 1]);
		_reactants.push_back(sub);
	}

	for (unsigned i = 0; i < products.size(); i += 2) {
		Product sub;
		sub.setCoefficient(atoi(products[i].c_str()));
		sub.setName(products[i + 1]);
		_products.push_back(sub);
	}
}
