#include <iostream>
#include <list>
#include <queue>
#include <stack>
#include <set>
#include <vector>
#include <stdlib.h>

#include "files.h"
#include "graph.h"

using namespace std;
const std::string Graph::N2N = "N2N"; //List of edges - Node by Node
const std::string Graph::REC = "REC"; //List of Reactions
const int DEFAULT_COST = 0;
/***********************************************/
/*                                             */
/* Definition of the Edge class                */
/*                                             */
/***********************************************/
Edge::Edge(Node *firstNode, Node *secondNode, int inCost) {
	this->orgNode = firstNode;
	this->dstNode = secondNode;
	cost = inCost;
}

Edge::Edge(string& orgNode, string& dstNode, int cost) {
	// e has the format as "ATP NADH 3" in which 3 is the cost
	Node* n1 = new Node(orgNode);
	Node* n2 = new Node(dstNode);
	this->orgNode = n1;
	this->dstNode = n2;
	this->cost = cost;
}

Edge::~Edge() {
	delete this->orgNode;
	delete this->dstNode;
}

Node* Edge::getDstNode() {
	return this->dstNode;
}

Node* Edge::getOrgNode() {
	return this->orgNode;
}

int Edge::getCost() {
	return this->cost;
}

void Edge::traverse() {
	cout << this->orgNode->getName() << " " << this->dstNode->getName() << endl;
}

void Edge::display() {
	cout << this->orgNode->getName() << " " << this->dstNode->getName() << endl;
}

/***********************************************/
/*                                             */
/* Definition of the Node class                */
/*                                             */
/***********************************************/
// An object of this class holds a vertex of the graph
Node::Node(std::string name) {
	name_ = name;
	status_ = NOT_VISITED;
	type_ = -1;
}

Node::~Node() {
	adjNodeList.clear();
}

bool Node::isAdjacent(Node* v) {
	vector<Node*> neighbors = this->getNeighbors();

	for (unsigned i = 0; i < neighbors.size(); i++)
		if (strcasecmp(neighbors[i]->getName().c_str(), v->getName().c_str())
		        == 0)
			return true;
	return false;
}

enum Status Node::getStatus() {
	return status_;
}

void Node::setStatus(enum Status st) {
	status_ = st;
}

string Node::getName() {
	return name_;
}

void Node::setName(string name) {
	this->name_ = name;
}

unsigned Node::getDegree() {
	return this->_degree;
}

void Node::setDegree(unsigned degree) {
	this->_degree = degree;
}

float Node::getEccentricity() {
	return this->_eccentricity;
}

void Node::setEccentricity(float value) {
	this->_eccentricity = value;
}

float Node::getCloseness() {
	return this->_closeness;
}

void Node::setCloseness(float value) {
	this->_closeness = value;
}

void Node::addAdjNode(Node *node, int cost) {
	// create an edge with 'this' as the originating node and
	// adjacency as the destination node
	if (!isAdjacent(node))
		adjNodeList.push_back(node);
}

void Node::removeAdjNode(Node* node) {
	vector<Node*>::iterator it;
	for (it = adjNodeList.begin(); it != adjNodeList.end(); ++it) {
		string s1 = (*it)->getName().c_str();
		string s2 = node->getName().c_str();
		if (s1.compare(s2) == 0) {
			//cout <<"1.1.1 Vao vung removeAdjNode..." <<endl;
			adjNodeList.erase(it);
			break;
		}
	}
	//cout <<"Da xoa xong dinh nay" <<endl;
}

vector<Node*>& Node::getAdjNodeList() {
	// return the started address of the list of nodes
	return adjNodeList;
}

vector<Node*> Node::getNeighbors() {
	vector<Node*> neighbors;
	for (unsigned i = 0; i < adjNodeList.size(); i++) {
		neighbors.push_back(adjNodeList[i]);
	}
	return neighbors;
}

unsigned Node::numberOfAdjacentNodes() {
	return adjNodeList.size();
}

unsigned Node::degree() {
	return adjNodeList.size();
}

// display all adjacent vertices of this vertex
void Node::traverse() {
	std::string edgeOp = " -> ";
	cout << name_;
	vector<Node*>::iterator it;
	for (it = adjNodeList.begin(); it != adjNodeList.end(); ++it) {
		cout << edgeOp << (*it)->getName();
	}
	cout << endl;
}

void Node::display() {
	// Use for testing
	string st = status(this->status_);
	cout << this->name_ << " " << st << " " << this->type_ << " " << this
	        << endl;
}

/***********************************************/
/*                                             */
/* Definition of the Graph class               */
/*                                             */
/***********************************************/
Graph::Graph() {
	desiredCycSize = 0;
	foundCycle = false;
}

Graph::Graph(std::string& file_name, const std::string format) {
	desiredCycSize = 0;
	foundCycle = false;
	if (!format.compare(Graph::N2N)) {
		std::vector<std::string> edges = load_edges(file_name);
		for (unsigned i = 0; i < edges.size(); i++) {
			if (edges[i].size()) {
				vector<string> ve = tokenizer(edges[i]);
				Node* n1 = this->findNodeByName(ve[0]);
				Node* n2 = this->findNodeByName(ve[1]);
				if (n1) {
					n1 = new Node(ve[0]);
					this->addNode(n1);
				}
				if (n2) {
					n2 = new Node(ve[1]);
					this->addNode(n2);
				}
				n1->addAdjNode(n2, DEFAULT_COST);
				n2->addAdjNode(n1, DEFAULT_COST);
				Edge* e = new Edge(n1, n2, DEFAULT_COST);
				this->addEdge(e);
			}
		}
	}
	else {
		std::vector<std::string> reactions = load_reactions(file_name);
		for (unsigned i = 0; i < reactions.size(); i++) {
			Reaction* r = new Reaction();
			r->createReaction(reactions[i]);
			this->createFromReaction(r);
		}
	}
}

Graph::Graph(unsigned vertexCount) {
	desiredCycSize = 0;
	foundCycle = false;
	cout << "So dinh:" << vertexCount << endl;
}

Graph::~Graph() {
	// free the memory space that allocated to store the vertices and the edges
	for (unsigned i = 0; i < nodeList.size(); i++)
		delete nodeList[i];
	for (unsigned i = 0; i < edgeList.size(); i++)
		delete edgeList[i];

	nodeList.clear();
	edgeList.clear();
}

vector<Node*> Graph::getNodes() {
	return this->nodeList;
}

vector<Edge*> Graph::getEdges() {
	return this->edgeList;
}

unsigned Graph::numberVertices() {
	return this->nodeList.size();
}

unsigned Graph::numberEdges() {
	return this->edgeList.size();
}

void Graph::clearVisited() {
	for (unsigned i = 0; i < nodeList.size() && !foundCycle; i++) {
		nodeList[i]->setStatus(NOT_VISITED);
	}
}

void Graph::addNode(Node* nNode) {
	if (!this->findNodeByName(nNode->getName()))
		nodeList.push_back(nNode);
}

void Graph::addNode(std::string& name) {
	Node* node = new Node(name);
	this->addNode(node);
}

void Graph::addEdge(Edge* nEdge) {
	string orgNodeName = nEdge->getOrgNode()->getName();
	string dstNodeName = nEdge->getDstNode()->getName();

	if (!findEdgeByNames(orgNodeName, dstNodeName)) {
		edgeList.push_back(nEdge);
	}
	Node* orgNode = nEdge->getOrgNode();
	Node* dstNode = nEdge->getDstNode();
	if (!findNodeByName(orgNode->getName()))
		this->addNode(orgNode);
	if (!findNodeByName(dstNode->getName()))
		this->addNode(dstNode);
	// Set the two nodes adjacent
	orgNode->addAdjNode(dstNode, DEFAULT_COST);
	dstNode->addAdjNode(orgNode, DEFAULT_COST);
}

void Graph::addEdge(std::string& nameNode1, std::string& nameNode2) {
	Node* node1 = findNodeByName(nameNode1);
	if (node1 == NULL)
		node1 = new Node(nameNode1);
	Node* node2 = findNodeByName(nameNode2);
	if (node2 == NULL)
		node2 = new Node(nameNode2);
	Edge* edge = new Edge(node1, node2, DEFAULT_COST);
	this->addEdge(edge);
}

void Graph::addEdge(Node* n1, Node* n2) {
	Edge* edge = this->findEdgeByNames(n1->getName(),n2->getName());
	if (edge)
		cout <<"Existing!" <<endl;
}

void Graph::removeNode(std::string& nodeName) {
	removeNode(this->findNodeByName(nodeName));
}

void Graph::removeNode(Node* nNode) {
	if (nNode) {
		// Remove it out its neighbors
		std::vector<Node*> adjnodelist = nNode->getAdjNodeList();
		std::vector<Node*>::iterator it;

		for (it = adjnodelist.begin(); it != adjnodelist.end(); ++it) {
			(*it)->removeAdjNode(nNode);
		}

		// Clear its adjacent list nodes
		nNode->getAdjNodeList().clear();
		// Remove this node out nodeList of the Graph Object
		for (it = nodeList.begin(); it != nodeList.end(); ++it) {
			string s1 = (*it)->getName().c_str();
			string s2 = nNode->getName().c_str();
			if (s1.compare(s2) == 0) {
				nodeList.erase(it);
				break;
			}
		}
	}
}

void Graph::removeEdge(Edge* edge) {
	Node* orgNode = edge->getOrgNode();
	Node* dstNode = edge->getDstNode();
	if (orgNode->degree() == 0)
		delete orgNode;
	if (dstNode->degree() == 0)
		delete dstNode;
	vector<Edge*>::iterator pos = edgeList.begin();
	bool found = false;
	for (pos = edgeList.begin(); pos != edgeList.end(); ++pos)
		if (*pos == edge) {
			found = true;
			break;
		}
	if (found) // == vector.end() means the element was not found
		edgeList.erase(pos);
}

Graph* Graph::extractReactionGraph() {
	/*
	 vector<Node*>::iterator itNodes;
	 vector<Node*> reactNodes;
	 for (itNodes = nodeList.begin(); itNodes != nodeList.end(); ++itNodes) {
	 if (((*itNodes)->getType().compare("REVENZ")==0) ||
	 ((*itNodes)->getType().compare("IRRREVENZ")==0))
	 reactNodes.push_back(*itNodes);
	 }*/
	return this;
}

Graph* Graph::extractMetaboliteGraph() {
	/*
		for (unsigned i = 0; i < this->reactions.size(); i++) {
			Reaction* r = new Reaction();
			r->createReaction(reactions[i]);
			r->display();
			cout <<&r <<endl;
			this->createMetaboliteGraphFromReaction(r);
		}
		this->traverse(1);
		set<unsigned> loop = this->hasDuplicatedEdges();
		if (loop.size())
			cout <<"Co canh lap lai" <<endl;
			*/
	return this;
}

vector<Node*>& Graph::sortNodesByEccentricity() {
	vector<Node*>& nodes = nodeList;
	unsigned j, k, n = nodes.size();
	for (j = 0; j < n; j++)
		for (k = j + 1; k < n; k++)
			if (nodes[j]->getEccentricity() < nodes[k]->getEccentricity())
				swap(nodes[j], nodes[k]);
	return nodes;
}

vector<Node*>& Graph::sortNodesByDegree() {
	vector<Node*>& nodes = nodeList;
	unsigned j, k, n = nodes.size();
	for (j = 0; j < n; j++)
		for (k = j + 1; k < n; k++)
			if (nodes[j]->getDegree() < nodes[k]->getDegree())
				swap(nodes[j], nodes[k]);
	return nodes;
}

vector<Node*>& Graph::sortNodesByName() {
	vector<Node*>& nodes = nodeList;
	unsigned j, k, n = nodes.size();
	for (j = 0; j < n; j++)
		for (k = j + 1; k < n; k++)
			if (strcasecmp(nodes[j]->getName().c_str(),
			        nodes[k]->getName().c_str()) > 0)
				swap(nodes[j], nodes[k]);
	return nodes;
}

Node* Graph::findNodeByName(string name) {
	for (unsigned i = 0; i < nodeList.size(); i++) {
		if (strcasecmp(nodeList[i]->getName().c_str(), name.c_str()) == 0)
			return (Node*) nodeList[i];
	}
	return NULL;
}

Edge* Graph::findEdgeByNames(string originalVertexName,
        string targetVertexName) {
	if (this->edgeList.size() != 0) {
		vector<Edge*>::iterator it = this->edgeList.begin();
		bool found1 = false;
		bool found2 = false;

		for (; it != this->edgeList.end(); ++it) {
			string sN1 = (*it)->getOrgNode()->getName().c_str();
			string sN2 = (*it)->getDstNode()->getName().c_str();
			string s1 = originalVertexName.c_str();
			string s2 = targetVertexName.c_str();
			found1 = s1.compare(sN1) == 0 && s2.compare(sN2) == 0;
			found2 = s2.compare(sN1) == 0 && s1.compare(sN2) == 0;
			if (found1 || found2)
				return *it;
		}
	}
	return NULL;
}

unsigned* Graph::degrees() {
	unsigned* degs = new unsigned[nodeList.size()];
	for (unsigned i = 0; i < nodeList.size(); i++) {
		degs[i] = nodeList[i]->degree();
		nodeList[i]->setDegree(degs[i]);
	}
	return degs;
}

void Graph::readFlightSchedules() {
	unsigned numOfCities, numOfFlights, cycSize;
	// read in number of cities, number of edges and the desired tour size
	cin >> numOfCities >> numOfFlights >> cycSize;

	while (numOfFlights--) {
		string fromCity, toCity;
		unsigned cost;

		cin >> fromCity >> toCity >> cost;
		// find if a vertex for the city already exists, if so get that
		Node* u = findNodeByName(fromCity);
		if (u == NULL) {
			u = new Node(fromCity);
			addNode(u);
		}

		// find if a vertex for the city already exists, if so get that
		Node* v = findNodeByName(toCity);
		if (v == NULL) {
			v = new Node(toCity);
			addNode(v);
		}

		u->addAdjNode(v, cost);
	}
	desiredCycSize = cycSize;
}

void Graph::createFromReaction(Reaction* r) {
	// create nodes: there is two kind of nodes: reaction node and molecule node
	// Determine the node either existing or not
	Node* rNode = this->findNodeByName(r->getName());
	if (!rNode) {
		rNode = new Node(r->getName());
		this->addNode(rNode);
	}

	// add all molecule nodes that created from the reactants
	vector<Reactant> reactants = r->getReactants();
	for (unsigned i = 0; i < reactants.size(); i++) {
		Node* mNode = this->findNodeByName(reactants[i].getName());
		if (!mNode) {
			mNode = new Node(reactants[i].getName());
			this->addNode(mNode);
		}
		// Create the respective edge and add it to the edge list
		// We don't need to verify the existent of the edge because it has created from
		// the unique reaction in the previous step. By default, the reactions in the data
		// file are different and not identical

		Edge* edge = new Edge(rNode, mNode, DEFAULT_COST);
		rNode->addAdjNode(mNode, DEFAULT_COST);
		mNode->addAdjNode(rNode, DEFAULT_COST);
		this->addEdge(edge);
	}
	// add all molecule nodes that created from the products
	vector<Product> products = r->getProducts();
	for (unsigned i = 0; i < products.size(); i++) {
		Node* pNode = this->findNodeByName(products[i].getName());
		if (!pNode) {
			pNode = new Node(products[i].getName());
			this->addNode(pNode);
		}

		// We also explain similarly as the above fashion
		Edge* edge = new Edge(rNode, pNode, DEFAULT_COST);
		rNode->addAdjNode(pNode, DEFAULT_COST);
		pNode->addAdjNode(rNode, DEFAULT_COST);
		this->addEdge(edge);
	}
}

void Graph::createFromReaction(std::string& reaction) {
	Reaction* r = new Reaction();
	r->createReaction(reaction);
	createFromReaction(r);
}

void Graph::createMetaboliteGraphFromReaction(Reaction* r) {
	// add all molecule nodes that created from the reactants
	vector<Reactant> reactants = r->getReactants();
	vector<Node*> recNodes;
	unsigned k = 0;
	for (unsigned i = 0; i < reactants.size(); i++) {
		Node* mNode = this->findNodeByName(reactants[i].getName());
		if (!mNode) {
			mNode = new Node(reactants[i].getName());
			this->addNode(mNode);
		}
		recNodes.push_back(mNode);
	}
	// Tao edge giua cac dinh nay vi cung 1 phan ung
	if (recNodes.size() > 1)
	for (unsigned i = 0; i < recNodes.size() - 1; i++)
		for (unsigned j = i + 1; j < recNodes.size(); j++)
			if (!findEdgeByNames(recNodes[i]->getName(),
			        recNodes[j]->getName())) {
				Edge* edge = new Edge(recNodes[i], recNodes[j], DEFAULT_COST);
				recNodes[i]->addAdjNode(recNodes[j], DEFAULT_COST);
				recNodes[j]->addAdjNode(recNodes[i], DEFAULT_COST);
				this->addEdge(edge);
				k++;
			}
	//cout <<k <<endl;
	// add all molecule nodes that created from the products
	vector<Product> products = r->getProducts();
	vector<Node*> proNodes;
	for (unsigned i = 0; i < products.size(); i++) {
		Node* pNode = this->findNodeByName(products[i].getName());
		if (!pNode) {
			pNode = new Node(products[i].getName());
			this->addNode(pNode);
		}
		proNodes.push_back(pNode);

	}
	// We also explain similarly as the above fashion
	// Tao edge giua cac dinh nay vi cung 1 phan ung
	k =0;
	if (proNodes.size()>1)
	for (unsigned i = 0; i < proNodes.size() - 1; i++)
		for (unsigned j = i + 1; j < proNodes.size(); j++)
			if (!findEdgeByNames(proNodes[i]->getName(),
			        proNodes[j]->getName())) {
				Edge* edge = new Edge(proNodes[i], proNodes[j], DEFAULT_COST);
				proNodes[i]->addAdjNode(proNodes[j], DEFAULT_COST);
				proNodes[j]->addAdjNode(proNodes[i], DEFAULT_COST);
				this->addEdge(edge);
				k++;
			}
	//cout <<k <<endl;

	k = 0;
	// Tao edge giua cac dinh reactants va products
	for (unsigned i = 0; i < recNodes.size(); i++)
		for (unsigned j = 0; j < proNodes.size(); j++)
			if (!findEdgeByNames(recNodes[i]->getName(),
			        proNodes[j]->getName())) {
				Edge* edge = new Edge(recNodes[i], proNodes[j], DEFAULT_COST);
				recNodes[i]->addAdjNode(proNodes[j], DEFAULT_COST);
				proNodes[j]->addAdjNode(recNodes[i], DEFAULT_COST);
				this->addEdge(edge);
				k++;
			}
	//cout <<k <<endl;
}

void Graph::createMetaboliteGraphFromReaction(std::string& reaction) {
	Reaction* r = new Reaction();
	r->createReaction(reaction);
	createMetaboliteGraphFromReaction(r);
}

void Graph::createReactionGraphFrom2Reactions(Reaction* r1, Reaction* r2) {
	Node* nr1 = this->findNodeByName(r1->name());
	if (!nr1)  {
		nr1 = new Node(r1->name());
		this->addNode(nr1);
	}

	Node* nr2 = this->findNodeByName(r2->name());
	if (!nr2) {
		nr2 = new Node(r2->name());
		this->addNode(nr2);
	}

	if (nr1 && nr2) {
		this->addEdge(nr1,nr2);
	}
}

float* Graph::bfs_eccentricity_centrality() {
	float* c_ecc = new float[nodeList.size()];
	//vector<Node*>::iterator it;
	for (unsigned i = 0; i < nodeList.size(); i++) {
		//for (it=nodeList.begin(); it!=nodeList.end(); ++it){
		c_ecc[i] = single_vertex_bfs_eccentricity(nodeList[i]); //*it);
		nodeList[i]->setEccentricity(c_ecc[i]);
	}
	return c_ecc;
}

float Graph::single_vertex_bfs_eccentricity(Node* v) {
	float result = 0;

	//typedef std::map <std::string, float> MapType;
	MapType distance;

	for (unsigned i = 0; i < nodeList.size(); i++) {
		distance[nodeList[i]->getName()] = -1;
	}
	distance[v->getName()] = 0;

	// Iterate over the map and print out all key/value pairs.
	// Using a const_iterator since we are not going to change the values.
	/*
	 MapType::const_iterator end = data.end();
	 for (MapType::const_iterator it = data.begin(); it != end; ++it)
	 {
	 std::cout << "Who(key = first): " << it->first;
	 std::cout << " Score(value = second): " << it->second << '\n';
	 }*/

	list<Node*> L;
	L.push_back(v);
	while (!L.empty()) {
		Node* w = L.front();
		L.pop_front();

		for (unsigned i = 0; i < w->getNeighbors().size(); i++) {
			Node* x = w->getNeighbors()[i];
			if (distance[x->getName()] == -1) {
				distance[x->getName()] = distance[w->getName()] + 1;
				if (result < distance[x->getName()])
					result = distance[x->getName()];
				L.push_back(x);
			}
		}
	}
	return 1 / result;
}

// _closeness centrality

// betweenness centrality
float Graph::brandes_betweenness_centrality(bool normalized,
        bool weighted_edges) {
	// """Compute the betweenness centrality for nodes in G:
	// the fraction of number of shortest paths that pass through each node.

	// The keyword normalized (default=True) specifies whether the
	// betweenness values are normalized by b=b/(n-1)(n-2) where
	// n is the number of nodes in G.

	// The keyword weighted_edges (default=False) specifies whether
	// to use edge weights (otherwise weights are all assumed equal).

	// The algorithm is from
	// Ulrik Brandes,
	// A Faster Algorithm for Betweenness Centrality.
	// Journal of Mathematical Sociology 25(2):163-177, 2001.
	// http://www.inf.uni-konstanz.de/algo/publications/b-fabc-01.pdf
	unsigned n = this->numberVertices();
	float* cb = new float[n];
	float betweenness = 0;

	// initialise
	for (unsigned i = 0; i < n; i++)
		*cb++ = 0;

	// loops
	vector<Node*>::iterator it = this->nodeList.begin();
	while (it != this->nodeList.end()) {
		stack<float> S;
		MyMatrix P;
		//for (unsigned i=0; i<nodeList.size(); i++)
		//  P[nodeList[i]->getName()] = new list<MapType>();
		float* sigma = new float[n];

		for (unsigned i = 0; i < n; i++)
			*sigma++ = 0;
		//sigma[it] = 1;
		//vector<float*> d = new float[n];
		if (weighted_edges) { // Use Dijkstra's algorithm for shortest paths, modified from Eppstein
			// bla bla bla
		}
		else { // use BFS
		       // ...

		}
	}
	if (normalized)
		cout << "Nornamalized\n";
	if (weighted_edges)
		cout << "Weighted edges\n";

	return betweenness;
}

/*
 def brandes_betweenness_centrality(G,normalized=True,weighted_edges=False):
 import heapq
 betweenness=dict.fromkeys(G,0.0) # b[v]=0 for v in G
 for s in G:
 S=[]
 P={}
 for v in G:
 P[v]=[]
 sigma=dict.fromkeys(G,0)    # sigma[v]=0 for v in G
 D={}
 sigma[s]=1
 if not weighted_edges:  # use BFS
 D[s]=0
 Q=[s]
 while Q:   # use BFS to find shortest paths
 v=Q.pop(0)
 S.append(v)
 for w in G.neighbors(v):
 #                for w in G.adj[v]: # speed hack, exposes internals
 if w not in D:
 Q.append(w)
 D[w]=D[v]+1
 if D[w]==D[v]+1:   # this is a shortest path, count paths
 sigma[w]=sigma[w]+sigma[v]
 P[w].append(v) # predecessors
 else:  # use Dijkstra's algorithm for shortest paths,
 # modified from Eppstein
 push=heapq.heappush
 pop=heapq.heappop
 seen = {s:0}
 Q=[]   # use Q as heap with (distance,node id) tuples
 push(Q,(0,s,s))
 while Q:
 (dist,pred,v)=pop(Q)
 if v in D:
 continue # already searched this node.
 sigma[v]=sigma[v]+sigma[pred] # count paths
 S.append(v)
 D[v] = seen[v]
 #                for w in G.adj[v]: # speed hack, exposes internals
 for w in G.neighbors(v):
 vw_dist = D[v] + G.get_edge(v,w)
 if w not in D and (w not in seen or vw_dist < seen[w]):
 seen[w] = vw_dist
 push(Q,(vw_dist,v,w))
 P[w]=[v]
 elif vw_dist==seen[w]:  # handle equal paths
 sigma[w]=sigma[w]+sigma[v]
 P[w].append(v)


 delta=dict.fromkeys(G,0)
 while S:
 w=S.pop()
 for v in P[w]:
 delta[v]=delta[v]+\
                          (float(sigma[v])/float(sigma[w]))*(1.0+delta[w])
 if w != s:
 betweenness[w]=betweenness[w]+delta[w]

 # normalize
 if normalized:
 order=len(betweenness)
 if order <=2:
 return betweenness # no normalization b=0 for all nodes
 scale=1.0/((order-1)*(order-2))
 for v in betweenness:
 betweenness[v] *= scale

 return betweenness */

bool Graph::isEmpty() {
	return (nodeList.size() == 0 || edgeList.size() == 0) ? true : false;
}

set<unsigned> Graph::hasDuplicatedEdges() {
	set<unsigned> loopEdges;
	for (unsigned i = 0; i < edgeList.size() - 1; i++)
		for (unsigned j = i + 1; j < edgeList.size(); j++) {
			string org1 = edgeList[i]->getOrgNode()->getName();
			string dst1 = edgeList[i]->getDstNode()->getName();
			string org2 = edgeList[j]->getOrgNode()->getName();
			string dst2 = edgeList[j]->getDstNode()->getName();
			if (((strcasecmp(org1.c_str(), dst2.c_str()) == 0
			        && strcasecmp(org2.c_str(), dst1.c_str()) == 0))
			        || ((strcasecmp(org1.c_str(), org2.c_str()) == 0
			                && strcasecmp(dst1.c_str(), dst2.c_str()) == 0))) {
				// Testing
				/*
				 cout << i  << " " << edgeList[i]->getOrgNode()->getName() << " "
				 << edgeList[i]->getDstNode()->getName() << endl;
				 cout << j  << " " << edgeList[j]->getOrgNode()->getName() << " "
				 << edgeList[j]->getDstNode()->getName() << endl;
				 */
				// Adding the indices of the duplicated edges
				loopEdges.insert(j);
			}
		}
	return loopEdges;
}
// node classify
void Graph::classifyNodes(string &filename) {
	if (!this) {
		cout << "Graph is null." << endl;
		exit(1);
	}
	typedef std::map<string, string> MapType;
	MapType type_nodes;

	// Load file
	ifstream myfile(filename.c_str());
	string line;
	vector<string> v;

	if (myfile.is_open()) {
		while (line.compare("-ENZREV")) {
			getline(myfile, line);
			line = trim(line);
		}
		getline(myfile, line); // Data
		/* Treatment */
		line = trim(line);
		v = tokenizer(line);
		for (unsigned i = 0; i < v.size(); i++) {
			type_nodes.insert(
			        std::pair<std::string, string>(v[i], Molecule::REVENZ));
		}
		getline(myfile, line); // Empty line
		getline(myfile, line); // -ENZIRREV
		getline(myfile, line); // Data
		/* Treatment */
		line = trim(line);
		v = tokenizer(line);
		for (unsigned i = 0; i < v.size(); i++)
			type_nodes.insert(std::make_pair(v[i], Molecule::IRRENZ));

		getline(myfile, line); // Empty line
		getline(myfile, line); // -METINT
		getline(myfile, line); // Data
		/* Treatment */
		line = trim(line);

		v = tokenizer(line);
		for (unsigned i = 0; i < v.size(); i++)
			type_nodes.insert(
			        std::pair<std::string, string>(v[i], Molecule::INTMET));

		getline(myfile, line); // Empty line
		getline(myfile, line); // -METEXT
		getline(myfile, line); // Data
		/* Treatment */
		line = trim(line);
		v = tokenizer(line);
		for (unsigned i = 0; i < v.size(); i++)
			type_nodes.insert(
			        std::pair<std::string, string>(v[i], Molecule::EXTMET));

		myfile.close();

		/* Phan loai cac node */
		MapType::iterator iter = type_nodes.begin();/*
		 for (iter=type_nodes.begin(); iter!=type_nodes.end(); iter++) {
		 Node* node = this->findNodeByName((*iter).first);
		 if (node)
		 node->setType((*iter).second);
		 }*/

		//iter = type_nodes.begin();
		for (unsigned i = 0; i < this->nodeList.size(); i++) {
			iter = type_nodes.find(nodeList[i]->getName());
			if (iter != type_nodes.end())
				nodeList[i]->setType(iter->second);
		}
		// clear the entries in the map
		type_nodes.clear();
	}
	else
		cout << "Unable to open file";
}

void Graph::traverse(int dd) {
	if (dd <= 0) { // Scan on the list of nodes
		for (unsigned i = 0; i < nodeList.size(); i++)
			nodeList[i]->traverse();
	}
	else if (dd == 1) { // Scan on the list of edges
		for (unsigned i = 0; i < edgeList.size(); i++)
			edgeList[i]->traverse();
	}
	else {
		for (unsigned i = 0; i < nodeList.size(); i++)
			nodeList[i]->traverse();
	}
}

MyMatrix Graph::generate_adjacent_matrix() {
	unsigned ROWS = this->nodeList.size();
	MapType d;
	MyMatrix v_matrix;

	for (unsigned i = 0; i < ROWS; i++) {
		d = this->adjacent_nodes(nodeList[i]);
		v_matrix[nodeList[i]->getName()] = d;
	}

	return v_matrix;
}

MyMatrix Graph::generate_distance_matrix() {
	unsigned ROWS = nodeList.size();
	MapType d;
	MyMatrix v_matrix;

	for (unsigned i = 0; i < ROWS; i++) {
		d = distance(nodeList[i]);
		v_matrix[nodeList[i]->getName()] = d;
	}

	return v_matrix;
}

// This function calculates the distance started from Node v to the rest of graph using BFS algorithm
MapType Graph::distance(Node * v) {
	unsigned result = 0;

	MapType distance_matrix;
	//unsigned* matrix = new unsigned[nodeList.size()];

	for (unsigned i = 0; i < nodeList.size(); i++) {
		distance_matrix[nodeList[i]->getName()] = -1;
	}
	distance_matrix[v->getName()] = 0;

	list<Node*> L;
	L.push_back(v);
	while (!L.empty()) {
		Node* w = L.front();
		L.pop_front();

		for (unsigned i = 0; i < w->getNeighbors().size(); i++) {
			Node* x = w->getNeighbors()[i];
			if (distance_matrix[x->getName()] == -1) {
				distance_matrix[x->getName()] = distance_matrix[w->getName()]
				        + 1;
				if (result < distance_matrix[x->getName()])
					result = distance_matrix[x->getName()];
				L.push_back(x);
			}
		}
	}

	return distance_matrix;
}

MapType Graph::adjacent_nodes(Node * v) {
	MapType adj_nodes;

	// Initialize
	for (unsigned i = 0; i < nodeList.size(); i++) {
		if (v->isAdjacent(nodeList[i]))
			adj_nodes[nodeList[i]->getName()] = 1;
		else
			adj_nodes[nodeList[i]->getName()] = 0;
	}
	return adj_nodes;
}

unsigned Graph::computeShortestPath(Node* start, Node* end) {
	cout << start->getEccentricity() << " " << end->getEccentricity() << endl;
	return 0;
}

void Graph::display() {
	cout << "The graph:\n";
	cout << "Number of vertices: " << this->numberVertices() << "\n";
	cout << "Number of edges: " << this->numberEdges() << "\n";
	cout << "The adjacency list: \n";
	traverse(1);
}

void Graph::displayEdgeList() {

}
void Graph::displayNodeList() {
	cout << "Name Status Type Memory" << endl;
	vector<Node*>::iterator it;
	for (it = nodeList.begin(); it != nodeList.end(); ++it)
		(*it)->display();
}

int Graph::findMinimalCutSet(Node* s, Node* t) {
	// Make a mean that follows the traces of visiting the nodes
	typedef std::map<Node*, bool> MapStringBoolean;
	MapStringBoolean checked;
	set<Node*> layer_i, layer_i_plus;
	for (unsigned i = 0; i < this->numberVertices(); i++)
		checked[nodeList[i]] = false;
	// Flag s as visited
	checked[s] = true;
	// Union s into LAYER_I
	layer_i.insert(s);
	int layer_i_index = 0;
	while (!layer_i.empty()) {
		/* Work with the LAYER_I and create a new LAYER_I_PLUS */
		layer_i_plus.clear();
		set<Node*>::iterator it = layer_i.begin();
		for (; it != layer_i.end(); it++) {
			Node* v = *(it);
			// Loop at all adjacent nodes w of v:
			vector<Node*> mtke = v->getNeighbors();
			vector<Node*>::iterator lit = mtke.begin();
			Node* w;
			for (; lit != mtke.end(); lit++) {
				w = *(lit);
				cout << "Dinh ke: " << w->getName() << endl;
				if (strcasecmp(w->getName().c_str(), v->getName().c_str()) == 0)
					continue;
				if (checked[w])
					continue;
				layer_i_plus.insert(w);
			}
		}

		/* Work with the LAYER_I_PLUS */
		if (layer_i_plus.empty()) {
			cout << "Rong\n";
			break;
		}

		it = layer_i_plus.begin();
		Node* v;
		cout << "The vertices in the layer i plus:\n";
		int count = 1;
		for (; it != layer_i_plus.end(); it++) {
			v = *(it);
			cout << v->getName() << endl;
			// Generate combinations of i vertices
			vector<Node*> vsets;
			vector<vector<int> > combs = combinations(layer_i_plus.size(),
			        count++);
			vector<vector<int> >::iterator it = combs.begin();
			for (; it != combs.end(); it++) {
				for (unsigned i = 0; i < it->size(); i++)
					cout << it->operator [](i) << ", ";
				cout << endl;
			}

			// Refactory
			/*      set<list>::iterator it;
			 for (it = combinations.begin(); it != combinations.end();  it++)
			 for (j = 0; j < it.count(); j++)
			 it[j] = layer_i_plus[it[j]];

			 // Recalculate the set VSETS
			 for (it = combinations.begin(); it != combinations.end();  it++) {
			 for (int j = 0; j < vsets[layer_i_index].count(); j++) {

			 }
			 }
			 */

			// Recalculate the LAYER_I
			layer_i_index++;
			layer_i.clear(); // LAYER_I = Empty
			/*
			 for (set<int>::iterator it = layer_i_plus.begin(), it != layer_i_plus.end(); it++) {
			 // LAYER_I = LAYER_I U {v}
			 layer_i.insert(v);
			 checked[v] = true;
			 }
			 */
		}
	} // end of while (LAYER_I != Empty)

	/* Verify that C is unique (minimal) */
	check_esets();
	return 1;
}
