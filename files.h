#ifndef _FILES_H_
#define _FILES_H_

#include <iostream>
#include <fstream>
#include <vector>
#include <set>

std::string& trim(std::string &str);

std::string& ltrim(std::string &str);

std::string& rtrim(std::string &str);

bool isNumber(const std::string& s);
/**
 * Token a string as reaction has form R : A + B = C + D .
 * @return A vector of sub strings, in this example: R A B C D
 */
std::vector<std::string> tokenizer(std::string& st);

std::vector<std::string> normalize_reaction(std::string& s);
std::vector<std::string> load_edges(std::string& from_file);
std::vector<std::string> load_reactions(std::string& from_file);

std::set<std::string> load_reversible_enzyme(std::string &filename);
std::set<std::string> load_irreversible_enzyme(std::string &filename);
std::set<std::string> load_internal_metabolite(std::string &filename);
std::set<std::string> load_external_metabolite(std::string &filename);
#endif
