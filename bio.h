#ifndef _BIO_H_
#define _BIO_H_

#include <iostream>
#include <string>
#include <map>
#include <list>
#include <set>
#include <vector>
#include <cctype>
#include <iomanip>
#include <string.h>
#include <sstream>

/** Used for
 *  holding a Molecule object
 */
class Molecule {
	private:
		int _coef;
		std::string _name;
		std::string _type;
	public:
		/*! A constructor with a string parameter.
		 *  It assigns this string to Molecule's name
		 */
		Molecule(const int coef, const std::string &name = "NO_NAME", const std::string type = Molecule::IRRENZ) :
				_coef(coef), _name(name), _type(type)  {
		}

		/*!
		 * Copy constructor
		 */
		Molecule(const Molecule &m) :
			_coef(m._coef),
			_name(m._name),
			_type(m._type)
			// We can defined this copy constructor as: *this = m replacing to three lines.
		{}

		//! Get the coefficient of a Molecule
		inline int getCoefficient() {
			return _coef;
		}

		//! Set the coefficient of a Molecule
		inline void setCoefficient(const int coef) {
			_coef = coef;
		}

		/*! Get the name of Molecule.
		 *  It gets the name of Molecule
		 */
		inline std::string getName() {
			return _name;
		}

		//! Set a string to the name of Molecule
		inline void setName(std::string name) {
			_name = name;
		}

		//! Get the type of Molecule. There are internal or external metabolite.
		inline std::string getType() {
			return _type;
		}

		//! Set a type as the type of Molecule
		inline void setType(std::string type) {
			_type = type;
		}

		//! Convert a type to string
		std::string toStringType();

		//! Convert a type to integer
		int convertType2String(std::string& type);

		static const std::string REVENZ; //!< Reversible Enzyme
		static const std::string IRRENZ; //!< Irreversible Enzyme
		static const std::string INTMET; //!< Internal Metabolite
		static const std::string EXTMET; //!< External Metabolite
		static const std::string METABOLITE; //!< Metabolite
		static const std::string REACTION; //!< Reaction
		static const std::string PRODUCT; //!< Product
		static const std::string REACTANT; //!< Reactant
};
/** Used for
 *  holding a Reactant object.
 */
class Reactant: public Molecule {
	public:
		//! The constructor with default parameters
		Reactant(const int coef=0, const std::string &name="NO_NAME", const std::string type=Molecule::REACTANT) :
			Molecule(coef, name, type) {
			setType(Molecule::REACTANT);
		}

		/*!
		 * Copy constructor
		 */
		Reactant(const Reactant &r) : Molecule (r) {}
};

/** Used for
 *  holding a Product object
 */
class Product: public Molecule {
	public:
		//! The constructor with default parameters
		Product(const int coef=0, const std::string &name="NO_NAME", const std::string type=Molecule::IRRENZ) :
			Molecule(coef, name, type) {
			setType(Molecule::PRODUCT);
		}

		/*!
		 * Copy constructor
		 */
		Product(const Product &p) : Molecule(p) {}
};

/** Used for
 *  holding a Reaction object
 */
class Reaction {
	public:
		//! A constructor without parameter
		Reaction() {
		}
		/**
		 * A constructor with parameters
		 * @param name The name of Reaction
		 * @param reactants The set of compounds as the left-side of the reaction
		 * @param products The set of compounds as the right-side of the reaction
		 */
		Reaction(std::string name, std::vector<Reactant> reactants,
		        std::vector<Product> products);
		/**
		 * A copy constructor
		 * @param r An reaction
		 */
		Reaction(Reaction &r);

		//! Get the name
		inline std::string getName() {
			return _name;
		}

		//! Get the name
		inline std::string name() {
			return _name;
		}

		/**
		 * Set the name
		 * @param name A string value as the name of Reaction
		 */
		inline void setName(std::string name) {
			_name = name;
		}
		/**
		 * Get all compounds at the left-side
		 * @return A vector is the set of reactants
		 */
		std::vector<Reactant> getReactants();
		/**
		 * Set the left-side of the reaction
		 * @param reactants A set of reactants
		 */
		void setReactants(std::vector<Reactant> reactants);
		/**
		 * Get the products of reaction
		 * @return A vector is the set of products
		 */
		std::vector<Product> getProducts();
		/**
		 * Set the right-side of the reaction
		 * @param products A set of products
		 */
		void setProducts(std::vector<Product> products);
		//! Display a reaction
		void display();
		//! Print the edges of graph
		void printEdges();
		/**
		 * Create a reaction from a string value
		 * @param s A string as the reactions, e.g., H + O2 -> H2O
		 */
		void createReaction(std::string s);
	private:
		std::string _name; //!< The name of reaction
		std::vector<Reactant> _reactants; //!< A set of reactants (the left-side)
		std::vector<Product> _products; //!< A set of products (the right-side)
};
extern std::map<std::string, const int> MoleculeTypes;
#endif
