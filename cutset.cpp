//================================================
// Name        	: cutset.cpp
// Author      		: Tung
// Version       	: 1.3.1
// Copyright   	: GNU
// Description  	: Graph Editor in C++, Ansi-style
//================================================

/** \mainpage BioCutSet Documentation
 *
 * \section intro_sec Introduction
 * This is the main point for involving the other functions. This project aims to
 * support for analyzing metabolic networks. At the first days, the program is
 * used to model a metabolic network as undirected graph. Structural
 * properties are then calculated on the graph. The properties are degree,
 * shortest path, centralities, etc.
 *
 * \section install_sec Installation
 * The program runs without installing. Run it directly from Terminal following
 * the propriety arguments (parameters)
 * \subsection step1 Step 1: Opening the box
 * \subsection step2 Step 2: make
 * \subsection step3 Step 3: make install
 * \section how_to_sec How To Run
 * \subsection compile_sec Compile: make
 * \subsection run_sec Run: ./cutset [options]
 */
#include <iostream>
#include <unistd.h> // getopt
#include <stdlib.h> // atoi
//#include <gtkmm.h>
//#include <QDate>
#include <QApplication>
#include <QDesktopWidget>
#include <QMenuBar>

#include "demo.h"
#include "mainwindow.h"


using namespace std;
/** Test
 * Main Point Program
 * @param argc: an integer as test case
 * @param argv[]: the options: a string as an input file name
 * @return 0/1: succeed or fail
 */

/*
 enum string_code {
 help,
 genAdjMatrix,
 calcCentrality,
 usage,
 };

 string_code hashit (std::string const& inString) {
 if ((inString == "--help") || (inString == "--usage") || (inString == "-h"))
 return help;
 if (inString == "gen -adjmat") return genAdjMatrix;

 if (inString == "calc -centrality") return calcCentrality;
 return usage;
 }

 */

void exclaim(int argc) {
	if (argc == 2) {
		cout << "You need to precise the data file name." << endl;
		exit(-1);
	}
}
void help() {
	cout << "Metabolic Network Analysis\n";
}

void guide(char const *cmd) {
	cout << "Try '" << cmd << " -h' for more information." << endl;
}

void usage(char const *cmd) {
	static char usage[] = " [-h][-f fname [-abcdefghijksrm]][-t fname]\n";
	static char fname[] = "\t-f fname loads the metabolic graph, where the fname is the name of a METATOOL file.\n";
	static char params[] = "\t-a shows the adjacent matrix\n"
			"\t-d show the distance matrix\n"
			"\t-e shows the edges list\n"
			"\t-k compute vertices degree \n"
			"\t-m extracts the metabolite graph: generating the edges list of this graph\n"
			"\t-r extracts the reaction graph: generating the edges list of this graph\n"
			"\t-t fname creates a reaction graph from the stoichiometric matrix contained in the fname file";
	cout << "usage: " << cmd << usage<<fname<<params <<endl;
}

int load_gui(int argc, char* argv[]) {
    //cout <<"Loading GUI...." <<endl;
    QApplication a(argc, argv);
    MainWindow w;

    //QString st = QDate::currentDate().toString();
    w.setWindowTitle("Hello CutSet");
    //w.adjustSize();
    w.menuBar()->setNativeMenuBar(false);
    w.move(QApplication::desktop()->screen()->rect().center() - w.rect().center());
    w.show();

    //QMessageBox msgBox;
    //msgBox.setText(st);
    //msgBox.exec();
    /*
    if (a.exec())
        cout <<"GUI loaded successully!" <<endl;
    else
        cout <<"Failed to load GUI!" <<endl;
    */
    return a.exec();
}

int main(int argc, char* argv[]) {
	/* Doan nay dung de tao GUI nhung chua chay tot */
	/*	Gtk::Main kit(argc, argv);
	 Gtk::Window window;
	 Gtk::Button button("Click here");

	 window.set_default_size(640, 480);
	 window.set_title("Gtkmm Programming");
	 window.add(button);
	 button.show();

	 Gtk::Main::run(window);*/

	//extern char *optarg;
	extern int optind;
	char c;
    int aflag = 0, bflag = 0, dflag = 0, fflag = 0, gflag = 0, hflag = 0, eflag = 0, kflag = 0, sflag = 0,
	        tflag = 0, rflag = 0, mflag = 0, err = 0;
	char const *fname;

	if (argc > 1) {
		while ((c = getopt(argc, argv, "abcdefghkstrm")) != -1)
			switch (c) {
				case 'a':
					aflag = 1;
					break;
				case 'b':
					bflag = 1;
					break;
				case 'd':
					dflag = 1;
					break;
                case 'g':
                    gflag = 1;
                    break;
				case 'h':
					hflag = 1;
					break;
				case 'e':
					eflag = 1;
					break;
				case 's':
					sflag = 1;
					break;
				case 'f':
					fflag = 1;
					fname = argv[optind];
					break;
				case 'k':
					kflag = 1;
					break;
				case 't':
					tflag = 1;
					break;
				case 'r':
					rflag = 1;
					break;
				case 'm':
					mflag = 1;
					break;
				case '?':
					err = 1;
					break;
			}

		if (hflag || err) {
			usage(argv[0]);
			exit(1);
		}

		if (fflag) {
			/* In this case, the program requires a source file that may be a graph
			 * file. This file must be .dat.
			 * Chuong trinh nay chua kiem tra dang file, van chay bua
			 * */
			if ((optind + 1) > argc) {
				/* need at least one argument (change +1 to +2 for two, etc. as needed) */
				printf("optind = %d, argc = %d\n", optind, argc);
				fprintf(stderr, "%s: missing argument \n", argv[0]);
				usage(argv[0]);
				exit(1);
			}
			// Do something here
			GraphApp* ga;
			ga = new GraphApp(fname);
			ga->initializeGraph();

			//cout <<"Test: Khoi tao duoc graph (du sai format van chay vao day). Lam the nao de pass qua cac loai graph?" <<endl;
			if (aflag) {
				cout << "The Adjacent Matrix" << endl;
				ga->generarate_adjacent_matrix();
			}

			if (bflag) {
				ga->test10();
			}

			if (dflag) {
				cout << "The Distance Matrix" << endl;
				ga->display_distance_matrix();
			}

			if (eflag) {
				cout << "The List of Edges" << endl;
				ga->get_list_of_edges();
			}

			if (kflag) {
				cout <<"Computing vertices degree" <<endl;
				ga->compute_degree();
			}

			if (sflag) {
				//ga->build_stoichiometric_matrix(m);
				//compare_degree();
				//ga->compute_degree_centrality();
				ga->check_duplicated_edges(fname);
				//ga->make_a_statistic_based_on_degree();
				//ga->compute_betweenness_centrality();
				//ga->compute_closeness_centrality();
				//create_graph_from_data_file(argv[2]);
				//test_classify_nodes();
			}

			if (rflag) {
				//cout <<"Reaction graph extracted!" <<endl;
				ga->extract_reaction_graph();
			}

			if (mflag) {
				ga->extract_metabolite_graph();
				//ga->compute_degree();
				//ga->test10();
			}
		}
        if (gflag) {
            load_gui(argc,argv);
        }
		if (tflag) {
			cout <<"Create the reaction graph from the stoichiometric matrix." <<endl;
			GraphApp* ga = new GraphApp();
			string filename = argv[optind];
			ga->build_reaction_graph_from_stoichiometric_matrix(filename);
			ga->remove_duplicated_edges();
			//ga->getGraphObject()->traverse(1);
			ga->test10();
		}
		if (kflag) {
			GraphApp* ga = new GraphApp();
			cout <<"Create a simple graph." <<endl;
			ga->create_simple_graph();
		}
	}
	else {
		cout << argv[0] << ": missing arguments" << endl;
		guide(argv[0]);
		exit(0);
	}

	return EXIT_SUCCESS;
}
