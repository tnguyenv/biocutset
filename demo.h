#ifndef DEMO_H
#define DEMO_H
#include "graph.h"
#include "files.h"
#include "utils.h"
#include "bio.h"

extern std::string FPCBMC_FILENAME;
/**
 * Used for managing a Graph Application.
 * A Graph Application includes a Graph object associated with a data file name.
 */
class GraphApp {
	private:
		std::string graphFile;		//!< This is metatool file
		Graph *g;
	public:
		/*!
		 * Constructor of GraphApp class: no parameter using for test stuffs
		 */
		GraphApp();
		/*! Constructor of GraphApp class.
		 *  It receives a metatool file
		 */
		GraphApp(const std::string graphFile);
		/*! Destructor
		 *
		 * */
		~GraphApp();

		/*! Get graph object of GraphApp
		 * */
		inline Graph* getGraphObject() {
			return g;
		}

		/**
		 * Generating the list of reactions
		 */
		std::vector<std::string> get_list_reactions();

		/**
		 * Getting the list of edges.
		 **/
		void get_list_of_edges();

		/**
		 * Initializing our graph
		 */
		Graph* initializeGraph();

		/**
		 * Initializing a graph basing on input file.
		 **/
		Graph* initialize(int n);

		/**
		 * Example: Hello World!
		 */
		void hello();

		/**
		 * Support information
		 */
		void help();

		/////////// TEST: CREATE GRAPH COMPUTATON TEST CASES ///////////
		/**
		 * Displaying basic information
		 **/
		void display_basic_information();

		/**
		 * Building the stoichiometric matrix
		 **/
		void build_stoichiometric_matrix(int);
		/**
		 * Building the reaction graph from the stoichiometric matrix file.
		 **/
		void build_reaction_graph_from_stoichiometric_matrix(const std::string & filename);
		/**
		 * Checking whether graph has duplicated edges or not. The graph is current.
		 **/
		void check_duplicated_edges();
		/**
		 * Checking whether graph has duplicated edges or not.
		 * @param filename is the input file name which the list of edges
		 **/
		void check_duplicated_edges(std::string filename);
		/**
		 * Removing the duplicated edges out of the graph.
		 **/
		void remove_duplicated_edges();
		/**
		 * Computing the betweenness centrality of the graph.
		 **/
		void compute_betweenness_centrality();
		/**
		 * Compute closeness centrality
		 * @return An integer as the order of input files
		 **/
		void compute_closeness_centrality();
		/**
		 * Compute degree centrality
		 * @return An integer as the order of input files
		 **/
		void compute_degree_centrality();
		/**
		 * Computing the degree values.
		 * @ return A list of the integers [0, 1, 2, ...] being the vertex degrees
		 **/
		void compute_degree();
		/**
		 * Computing the eccentricity centrality.
		 **/
		void compute_eccentricity_centrality();
		/**
		 * Creating a graph from a data file.
		 * @param filename is the data file name.
		 **/
		void create_graph_from_data_file(std::string filename);
		/**
		 * Displaying the distance matrix of the current graph.
		 **/
		void display_distance_matrix();
		/**
		 * Displaying the list of reactions of the metabolic network that modelled by the current graph.
		 **/
		void display_list_reactions();
		/**
		 * Making a statistic using the degree-based properties.
		 **/
		void make_a_statistic_based_on_degree();

		/**
		 * Generating the adjacent matrix.
		 **/
		void generarate_adjacent_matrix();

		/////////// TEST: CREATE GRAPH TEST CASES ///////////
		/**
		 * Creating manually a graph: add a node, add an edge, remove a node, remove an edge, etc.
		 **/
		void create_graph_manually();
		/**
		 * Creating manually a graph from reactions. Adding the reaction into the graph. Finding an edge. Removing a node.
		 **/
		void create_simple_graph();

		/**
		 * Extracting the metabolite graph.
		 * @return Null
		 **/
		void extract_metabolite_graph();

		/**
		 * Extracting the reaction graph.
		 * @return Null
		 **/
		void extract_reaction_graph();


		/////////// TEST: CREATE LEMON LIBRARY TEST CASES ///////////
		/**
		 * Finding all cut sets using the algorithm in lemon library.
		 **/
		void find_all_cutsets();
		/**
		 * Testing the lemon library.
		 **/
		void use_lemon();
		/**
		 * Trying using the linear optimization algorithms: LP
		 **/
		void lp_demo();
		/**
		 * Calculate the minimum cut set of a directed graph.
		 * @param input_file The input file formated LEMON standard.
		 */
		void hao_orlin_demo(std::string input_file);
		/**
		 * Testing Gomory Hu's algorithm.
		 **/
		void gomory_hu_demo();

		/////////// TEST: CREATE GRAPH TEST CASES WITH BOOST LIBRARY ///////////
		void boost_demo1();
		void stoer_wagner();

		/////////// TEST: STUFFS TEST CASES ///////////
		void test_map();
		void test_map_2();
		void test_map_3();
		void test_hash_map();
		void test_sort();
		void test1();
		void test2();
		void test3();
		void test4();
		void test5();
		void test6();
		void test7();
		void test8();
		void test9();
		void test10();
		void merge2lists();
		void test_Tableclass();
		void compare_degree();
		void test_set();
		void test_classify_nodes();
		void foo(int& y);
		void test_reference_parameter_in_c2plus();
		void printvector(std::vector<std::string>);
};
#endif // DEMO_H
