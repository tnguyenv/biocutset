/**
 * This section consists of all demonstrative programs which are used to test and to verify the defined functions.
 */
#include <iostream>
#include<fstream>

#include <map>
#include <utility>
#include <algorithm>
#include <set>
#include <string>
#include <stdlib.h>
#include <cstddef>
#include <stdio.h>
#include "demo.h"
#include "files.h"

//#include <lemon/lp.h>
//#include <lemon/smart_graph.h>
//#include <lemon/adaptors.h>
//#include <lemon/concepts/digraph.h>
//#include <lemon/concepts/maps.h>
//#include <lemon/lgf_reader.h>
//#include <lemon/hao_orlin.h>

//using namespace lemon;
using namespace std;

string PATH = "/home/nvntung/Dropbox/these/data/";

string FPCBMC_FILENAME = PATH + "fpcBMC.dat";
string FPCBMC_FILENAME_N2N = PATH + "FPCBMC-n2n";
string FPCBMC_META = PATH + "FPCBMCMetabo-sort";
string FPCBMC_META_NEW = PATH + "FPCBMCMetabo-stoi-intmet";
string FPCBMC_REAC = PATH + "FPCBMCReact-sort";
string FPCBMC_META_INT = PATH + "FPCBMCMetabo-sort-intmet";
string FPCBMC_META_GEN_EXT_MET = PATH + "FPCBMCMetabo-sort-genmet";
string FPCBMC_CASE3 = PATH + "FPCBMC-case3";

string ARC_FILENAME = PATH + "aracell.dat";
string ARC_FILENAME_N2N = PATH + "Aracell-n2n";
string ARC_META = PATH + "AracellMetabo-sort";
string ARC_META_NEW = PATH + "AracellMetabo-stoi-intmet";
string ARC_REAC = PATH + "AracellReact-sort";
string ARC_META_INT = PATH + "AracellMetabo-sort-intmet";
string ARC_META_GEN_EXT_MET = PATH + "AracellMetabo-sort-genmet";
string ARC_CASE3 = PATH + "Aracell-case3";

string ARC2_FILENAME = PATH + "aracell2.dat";
string ARC2_FILENAME_N2N = PATH + "Aracell2-n2n";
string ARC2_META = PATH + "Aracell2Metabo-sort";
string ARC2_META_NEW = PATH + "Aracell2Metabo-stoi-intmet";
string ARC2_REAC = PATH + "Aracell2React-sort";
string ARC2_META_INT = PATH + "Aracell2Metabo-sort-intmet";
string ARC2_META_GEN_EXT_MET = PATH + "Aracell2Metabo-sort-genmet";
string ARC2_CASE3 = PATH + "Aracell2-case3";

string MUSCLE_FILENAME = PATH + "mito-muscle-new.dat";
string MUSCLE_FILENAME_N2N = PATH + "Muscle-n2n";
string MUSCLE_META = PATH + "MuscleMetabo-new-sort";
string MUSCLE_META_NEW = PATH + "MuscleMetabo-stoi-intmet";
string MUSCLE_REAC = PATH + "MuscleReact-new-sort";
string MUSCLE_META_INT = PATH + "MuscleMetabo-new-sort-intmet";
string MUSCLE_META_GEN_EXT_MET = PATH + "MuscleMetabo-sort-genmet";
string MUSCLE_CASE3 = PATH + "Muscle-case3";
string SAMPLE_GRAPH = PATH.append("sample");

GraphApp::GraphApp() {
	this->g = new Graph();
	this->graphFile = "";
}

GraphApp::GraphApp(const std::string graphFile = FPCBMC_FILENAME) {
	this->g = new Graph();
	this->graphFile = graphFile;
}

void GraphApp::hello() {
	cout << "Hello World!" << endl;
}

void GraphApp::help() {
	cout << "Chuong trinh minh hoa Metabolic Network\n";
	cout << "Usage: cutset"
	        << " n m\nin which n = 1..11 (e.x., 1: test1, 2: test2...)\nm input file index";
	cout << "=================================================\n";
	cout << "1. Test cac stuffs.\n";
	cout << "2. Test function: create a graph.\n";
	cout << "3. Test xay dung stoichiometric matrix.\n";
	cout << "4. Test kiem tra cac canh co bi lap khong.\n";
	cout << "5. Tinh betweennes centrality.\n";
	cout << "6. Hien thi shortest path matrix.\n";
	cout << "7. Test cac map.\n";
	cout << "8. Tinh degree centrality.\n";
	cout << "9. Tinh eccentricity centrality.\n";
	cout << "10. Tinh closeness centrality.\n";
	cout << "=================================================\n";
}

Graph* GraphApp::initialize(int n) {
	Graph* g;

	switch (n) {
		case 1:
			//g = new Graph(FPCBMC_FILENAME,Graph::REC);
			//g = new Graph(FPCBMC_FILENAME_N2N,Graph::N2N);
			//g = new Graph(FPCBMC_META,Graph::N2N);
			//g = new Graph(FPCBMC_REAC,Graph::N2N);

			// 3 case studies
			//g = new Graph(FPCBMC_META_INT,Graph::N2N);
			//g = new Graph(FPCBMC_META_GEN_EXT_MET,Graph::N2N);
			//g = new Graph(FPCBMC_CASE3,Graph::REC);

			// stoichiometric matrix
			g = new Graph(FPCBMC_META_NEW, Graph::N2N);

			//g = new Graph(SAMPLE_GRAPH,Graph::N2N);
			g->classifyNodes(FPCBMC_FILENAME);
			break;
		case 2:
			//g = new Graph(ARC_FILENAME,Graph::REC);
			//g = new Graph(ARC_FILENAME_N2N,Graph::N2N);
			//g = new Graph(ARC_META,Graph::N2N);
			g = new Graph(ARC_REAC, Graph::N2N);

			// 3 case studies
			//g = new Graph(ARC_META_INT,Graph::N2N);
			//g = new Graph(ARC_META_GEN_EXT_MET,Graph::N2N);
			//g = new Graph(ARC_CASE3,Graph::REC);

			// stoichiometric matrix
			//g = new Graph(ARC_META_NEW,Graph::N2N);
			g->classifyNodes(ARC_FILENAME);
			break;
		case 3:
			//g = new Graph(ARC2_FILENAME,Graph::REC);
			//g = new Graph(ARC2_FILENAME_N2N,Graph::N2N);
			//g = new Graph(ARC2_META,Graph::N2N);
			g = new Graph(ARC2_REAC, Graph::N2N);

			// 3 case studies
			//g = new Graph(ARC2_META_INT,Graph::N2N);
			//g = new Graph(ARC2_META_GEN_EXT_MET,Graph::N2N);
			//g = new Graph(ARC2_CASE3,Graph::REC);

			// stoichiometric matrix
			//g = new Graph(ARC2_META_NEW,Graph::N2N);
			g->classifyNodes(ARC2_FILENAME);
			break;
		default:
			//g = new Graph(MUSCLE_FILENAME,Graph::REC);
			//g = new Graph(MUSCLE_FILENAME_N2N,Graph::N2N);
			//g = new Graph(MUSCLE_META,Graph::N2N);
			g = new Graph(MUSCLE_REAC, Graph::N2N);

			// 3 case studies
			//g = new Graph(MUSCLE_META_INT,Graph::N2N);
			//g = new Graph(MUSCLE_META_GEN_EXT_MET,Graph::N2N);
			//g = new Graph(MUSCLE_CASE3,Graph::REC);

			// stoichiometric matrix
			//g = new Graph(MUSCLE_META_NEW,Graph::N2N);
			g->classifyNodes(MUSCLE_FILENAME);
			break;
	}
	return g;
}

Graph* GraphApp::initializeGraph() {
	g = new Graph(this->graphFile, Graph::REC);
	g->classifyNodes(this->graphFile);
	return g;
}

void GraphApp::display_basic_information() {
	cout <<"Number of vertices: " <<this->g->numberVertices() <<endl;
	cout <<"Number of edges: " <<this->g->numberEdges() <<endl;
}

void GraphApp::display_distance_matrix() {
	this->initializeGraph();

	MyMatrix m = this->g->generate_distance_matrix();
	MyMatrix::iterator it = m.begin();
	MapType::iterator d;
	//  unsigned total_distance;

	for (it = m.begin(); it != m.end(); it++) {
		cout << (*it).first << " ";
		//    total_distance = 0;
		//    for (d=(*it).second.begin(); d!=(*it).second.end(); d++)
		//      //cout <<(*d).first <<"->" <<(*d).second <<" ";
		//      total_distance += (*d).second;
		//    cout <<(float)1/total_distance <<": " <<total_distance <<endl;
	}

	cout << endl;
	// Display distance matrix
	for (it = m.begin(); it != m.end(); it++) {
		//cout <<(*it).first <<": ";
		for (d = (*it).second.begin(); d != (*it).second.end(); d++)
			//cout <<(*d).first <<"->" <<(*d).second <<" ";
			cout << (*d).second << " ";
		cout << endl;
	}
}

void GraphApp::display_list_reactions() {
	std::vector<std::string> lstReacts = this->get_list_reactions();
	std::vector<std::string>::iterator it = lstReacts.begin();
	for (it = lstReacts.begin(); it != lstReacts.end(); it++) {
		cout <<(*it) <<endl;
	}
}

void GraphApp::extract_reaction_graph() {
	// this->g->extractReactionGraph()->displayEdgeList(); implemented later
	// based on the works of Wagner and Fell (2006)
	Graph* rg = new Graph();
	std::vector<std::string> lstReacts = this->get_list_reactions();
	for (unsigned  i=0; i<lstReacts.size()-1; i++) {
		std::vector<std::string> r1 = tokenizer((lstReacts[i]));
		string reac1 = r1[0];
		sort(r1.begin(),r1.end());
		for (unsigned  j=i+1; j<lstReacts.size();j++) {
			std::vector<std::string> r2 = tokenizer((lstReacts[j]));
			string reac2 = r2[0];
			sort(r2.begin(),r2.end());
			std::vector<std::string> cc;
			set_intersection(r1.begin(),r1.end(),r2.begin(),r2.end(),back_inserter(cc));
			if (cc.size())
				rg->addEdge(reac1, reac2);
		}
	}

	set<unsigned> duplicatedEdgeIndices = rg->hasDuplicatedEdges();
	if (duplicatedEdgeIndices.size()) {
		vector<Edge*> edges = rg->getEdges();
		set<unsigned>::iterator it;
		for (it=duplicatedEdgeIndices.begin();it != duplicatedEdgeIndices.end(); ++it)
			rg->removeEdge(edges[*it]);
	}
	rg->traverse(1); // scanning the edges list
}
void GraphApp::printvector (std::vector<std::string> v) {
	vector<string>::iterator it=v.begin();
	for (vector<string>::iterator it=v.begin(); it != v.end(); it++)
		cout <<(*it) <<" ";
	cout <<endl;
}
void GraphApp::extract_metabolite_graph() {
	// this->g->extractMetaboliteGraph()->displayEdgeList(); implemented later
	// based on Wagner and Fell (2006)
	Graph* mg = new Graph();
	std::vector<std::string> lstReacts = this->get_list_reactions();
	std::vector<std::string>::iterator it = lstReacts.begin();
	for (it = lstReacts.begin(); it != lstReacts.end(); it++) {
		mg->createMetaboliteGraphFromReaction((*it));
	}
	set<unsigned> duplicatedEdgeIndices = mg->hasDuplicatedEdges();
	if (duplicatedEdgeIndices.size()) {
		vector<Edge*> edges = mg->getEdges();
		set<unsigned>::iterator it;
		for (it=duplicatedEdgeIndices.begin();it != duplicatedEdgeIndices.end(); ++it)
			mg->removeEdge(edges[*it]);
	}
	mg->traverse(1);
}

void GraphApp::get_list_of_edges() {
	set<unsigned> setLoopEdges = this->g->hasDuplicatedEdges();
	if (setLoopEdges.size())
		cout <<"The graph has several duplicated edges." <<endl;

	this->g->traverse(1); //!< 0: scan on list of edges
}

void GraphApp::generarate_adjacent_matrix() {
	MyMatrix m = this->g->generate_adjacent_matrix();
	MyMatrix::iterator it = m.begin();
	MapType::iterator d;
	//  unsigned total_distance;

	for (it = m.begin(); it != m.end(); it++) {
		cout << (*it).first << " ";
		//    total_distance = 0;
		//    for (d=(*it).second.begin(); d!=(*it).second.end(); d++)
		//      //cout <<(*d).first <<"->" <<(*d).second <<" ";
		//      total_distance += (*d).second;
		//    cout <<(float)1/total_distance <<": " <<total_distance <<endl;
	}

	cout << endl;
	// Display distance matrix
	for (it = m.begin(); it != m.end(); it++) {
		//cout <<(*it).first <<": ";
		for (d = (*it).second.begin(); d != (*it).second.end(); d++)
			//cout <<(*d).first <<"->" <<(*d).second <<" ";
			cout << (*d).second << " ";
		cout << endl;
	}

	cout << g->getNodes()[0]->getName() << endl;
	cout << g->findNodeByName("CO2")->getName() << endl;
	cout << g->getNodes()[0]->isAdjacent(g->findNodeByName("CO2")) << endl;
}

std::vector<std::string> GraphApp::get_list_reactions() {
	return load_reactions(this->graphFile);
}

void GraphApp::compute_degree_centrality() {
	this->g->degrees();
	this->g->bfs_eccentricity_centrality();

	vector<Node*> nodes = g->sortNodesByDegree(); // g->sortNodesByName();
	unsigned max = 0;
	string name = "";
	int totaldegree = 0;
	int revenz = 0, irrevenz = 0, intmet = 0, extmet = 0;
	unsigned no_revenz = 0, no_irrevenz = 0, no_intmet = 0, no_extmet = 0;
	vector<Node*> int_nodes;
	vector<Node*> ext_nodes;

	//Print degrees
	cout << "VertexName" << "\t" << "NodeType" << "\t" << "Degree" << "\t"
	        << "Eccentricity" << endl;
	for (unsigned i = 0; i < nodes.size(); i++) {
		string t = nodes[i]->getType();
		int type = MoleculeTypes[t];
		switch (type) {
			case 1:
				revenz += nodes[i]->degree();
				no_revenz++;
				break;
			case 2:
				irrevenz += nodes[i]->degree();
				no_irrevenz++;
				break;
			case 3:
				intmet += nodes[i]->degree();
				no_intmet++;
				int_nodes.push_back(nodes[i]);
				break;
			case 4:
				extmet += nodes[i]->degree();
				no_extmet++;
				ext_nodes.push_back(nodes[i]);
				break;
		}

		totaldegree += nodes[i]->degree();
		cout << nodes[i]->getName() << "\t" << nodes[i]->getType() << "\t"
		        << nodes[i]->degree() << "\t" << nodes[i]->getEccentricity()
		        << endl;
		if (nodes[i]->degree() > max) {
			max = nodes[i]->degree();
			name = nodes[i]->getName();
		}
	}

	cout << "\nAverage rev degree: " << (float) revenz / no_revenz
	        << " nb of nodes: " << no_revenz << endl;
	cout << "\nAverage irrev degree: " << (float) irrevenz / no_irrevenz
	        << " nb of nodes: " << no_irrevenz << endl;
	cout << "\nAverage int degree: " << (float) intmet / no_intmet
	        << " nb of nodes: " << no_intmet << endl;
	cout << "\nAverage ext degree: " << (float) extmet / no_extmet
	        << " nb of nodes: " << no_extmet << endl;
	cout << "\nAverage total degree: "
	        << (float) totaldegree / g->numberVertices() << endl;
	cout << "\nNumber of nodes: " << g->numberVertices() << endl;
	cout << "\nNumber of edges: " << g->numberEdges() << endl;
	cout << "\nMaximum degree: " << name << ": " << max << endl;
	cout << g->numberVertices() << "\t" << g->numberEdges() << "\t"
	        << no_revenz + no_irrevenz << "\t" << no_intmet + no_extmet << "\t"
	        << (float) totaldegree / g->numberVertices() << "\t"
	        << (float) (revenz + irrevenz) / (no_revenz + no_irrevenz) << "\t"
	        << (float) (intmet + extmet) / (no_intmet + no_extmet) << "\t"
	        << endl;
	cout << "\n============\n";
	if (int_nodes.size()) {
		for (unsigned i = 0; i < int_nodes.size(); i++) {
			vector<Node*> neighbors = int_nodes[i]->getNeighbors();
			for (unsigned j = 0; j < neighbors.size(); j++)
				cout << int_nodes[i]->getName() << " "
				        << neighbors[j]->getName() << endl;
		}
	}

	//  if (ext_nodes.size()) {

	//  }
}

void GraphApp::make_a_statistic_based_on_degree() {
	this->initializeGraph();
	this->g->degrees();

	vector<Node*> nodes = g->sortNodesByDegree(); // g->sortNodesByName();
	unsigned max = 0, max_rec = 0, min_rec = nodes.size(), max_int_met = 0,
	        min_int_met = nodes.size();
	string name = "", max_rec_name = "", min_rec_name = "", max_int_met_name =
	        "", min_int_met_name = "";
	int totaldegree = 0;
	int revenz = 0, irrevenz = 0, intmet = nodes.size(), extmet = 0;
	unsigned no_revenz = 0, no_irrevenz = 0, no_intmet = 0, no_extmet = 0;
	vector<Node*> int_nodes;
	vector<Node*> ext_nodes;

	//Print degrees
	//cout <<"VertexName" <<"\t" <<"NodeType" <<"\t" <<"Degree"<<"\t" <<"Eccentricity"<<endl;
	for (unsigned i = 0; i < nodes.size(); i++) {
		char type = MoleculeTypes[nodes[i]->getType()];
		switch (type) {
			case 1:
				revenz += nodes[i]->degree();
				no_revenz++;
				if (nodes[i]->degree() > max_rec) {
					max_rec = nodes[i]->degree();
					max_rec_name = nodes[i]->getName();
				}
				if (nodes[i]->degree() < min_rec) {
					min_rec = nodes[i]->degree();
					min_rec_name = nodes[i]->getName();
				}
				break;
			case 2:
				irrevenz += nodes[i]->degree();
				no_irrevenz++;
				if (nodes[i]->degree() > max_rec) {
					max_rec = nodes[i]->degree();
					max_rec_name = nodes[i]->getName();
				}
				if (nodes[i]->degree() < min_rec) {
					min_rec = nodes[i]->degree();
					min_rec_name = nodes[i]->getName();
				}
				break;
			case 3:
				intmet += nodes[i]->degree();
				no_intmet++;
				int_nodes.push_back(nodes[i]);
				if (nodes[i]->degree() > max_int_met) {
					max_int_met = nodes[i]->degree();
					max_int_met_name = nodes[i]->getName();
				}
				if (nodes[i]->degree() < min_int_met) {
					min_int_met = nodes[i]->degree();
					min_int_met_name = nodes[i]->getName();
				}
				break;
			case 4:
				extmet += nodes[i]->degree();
				no_extmet++;
				ext_nodes.push_back(nodes[i]);
				break;
		}

		totaldegree += nodes[i]->degree();
		//cout <<nodes[i]->getName() <<"\t" <<nodes[i]->getType() <<"\t" <<nodes[i]->degree() <<"\t" <<nodes[i]->getEccentricity() <<endl;
		if (nodes[i]->degree() > max) {
			max = nodes[i]->degree();
			name = nodes[i]->getName();
		}
	}

	unsigned max_met = 0, min_met = nodes.size();
	string max_met_name = "", min_met_name = "";
	if (int_nodes.size()) {
		for (unsigned i = 0; i < int_nodes.size(); i++) {
			if (int_nodes[i]->degree() > max_met) {
				max_met = int_nodes[i]->degree();
				max_met_name = int_nodes[i]->getName();
			}
			if (int_nodes[i]->degree() < min_met) {
				min_met = int_nodes[i]->degree();
				min_met_name = int_nodes[i]->getName();
			}
		}
	}
	if (ext_nodes.size()) {
		for (unsigned i = 0; i < ext_nodes.size(); i++) {
			if (ext_nodes[i]->degree() > max_met) {
				max_met = ext_nodes[i]->degree();
				max_met_name = ext_nodes[i]->getName();
			}
			if (ext_nodes[i]->degree() < min_met) {
				min_met = ext_nodes[i]->degree();
				min_met_name = ext_nodes[i]->getName();
			}
		}
	}
	cout << "\nAverage rev degree: " << (float) revenz / no_revenz
	        << " nb of nodes: " << no_revenz << endl;
	cout << "\nAverage irrev degree: " << (float) irrevenz / no_irrevenz
	        << " nb of nodes: " << no_irrevenz << endl;
	cout << "\nAverage int degree: " << (float) intmet / no_intmet
	        << " nb of nodes: " << no_intmet << endl;
	cout << "\nAverage ext degree: " << (float) extmet / no_extmet
	        << " nb of nodes: " << no_extmet << endl;
	cout << "\nAverage total degree: "
	        << (float) totaldegree / g->numberVertices() << endl;
	cout << "\nNumber of nodes: " << g->numberVertices() << endl;
	cout << "\nNumber of edges: " << g->numberEdges() << endl;
	cout << "\nMaximum degree: " << name << ": " << max << endl;
	cout << g->numberVertices() << "\t" << g->numberEdges() << "\t"
	        << no_revenz + no_irrevenz << "\t" << no_intmet + no_extmet << "\t"
	        << (float) totaldegree / g->numberVertices() << "\t"
	        << (float) (revenz + irrevenz) / (no_revenz + no_irrevenz) << "\t"
	        << (float) (intmet + extmet) / (no_intmet + no_extmet) << "\t"
	        << max_rec << "/" << max_rec_name << "\t" << min_rec << "/"
	        << min_rec_name << "\t" << max_met << "/" << max_met_name << "\t"
	        << min_met << "/" << min_met_name << "\t"
	        << (float) intmet / no_intmet << "\t" << max_int_met << "/"
	        << max_int_met_name << "\t" << min_int_met << "/"
	        << min_int_met_name << "\t" << endl;
}

void GraphApp::compute_eccentricity_centrality() {
	this->initializeGraph();
	// Method 1
	/*float* ecc = g->bfs_eccentricity_centrality();
	 vector<Node*> nodes = g->getNodes();
	 for (unsigned i=0; i<nodes.size(); i++)
	 cout <<nodes[i]->getName() <<": " <<*(ecc + i) <<endl;
	 */
	// Method 2
	this->g->bfs_eccentricity_centrality();
	//vector<Node*> nodes = g->sortNodesByEccentricity();
	//vector<Node*> nodes = g->sortNodesByName();
	vector<Node*> nodes = g->getNodes();
	float max = 0;
	string name = "";
	float totalEccentricity = 0.0;
	float revenz = 0.0, irrevenz = 0.0, intmet = 0.0, extmet = 0.0;
	unsigned no_revenz = 0, no_irrevenz = 0, no_intmet = 0, no_extmet = 0;

	for (unsigned i = 0; i < nodes.size(); i++) {
		char type = MoleculeTypes[nodes[i]->getType().c_str()];
		switch (type) {
			case 1:
				revenz += nodes[i]->getEccentricity();
				no_revenz++;
				break;
			case 2:
				irrevenz += nodes[i]->getEccentricity();
				no_irrevenz++;
				break;
			case 3:
				intmet += nodes[i]->getEccentricity();
				no_intmet++;
				break;
			case 4:
				extmet += nodes[i]->getEccentricity();
				no_extmet++;
				break;
		}

		totalEccentricity += nodes[i]->getEccentricity();
		// Display eccentricities
		cout << nodes[i]->getName() << ": " << nodes[i]->getType() << ": "
		        << nodes[i]->getEccentricity() << endl;
		if (max < nodes[i]->getEccentricity()) {
			max = nodes[i]->getEccentricity();
			name = nodes[i]->getName();
		}
	}
	cout << "\nAverage rev eccentricity: " << (float) revenz / no_revenz
	        << endl;
	cout << "\nAverage irrev eccentricity: " << (float) irrevenz / no_irrevenz
	        << endl;
	cout << "\nAverage int eccentricity: " << (float) intmet / no_intmet
	        << endl;
	cout << "\nAverage ext eccentricity: " << (float) extmet / no_extmet
	        << endl;
	cout << "\nAverage total eccentricity: "
	        << (float) totalEccentricity / g->numberVertices() << endl;
	cout << "\nMaximum value eccentricity: " << name << " : " << max << endl;
	cout << "\nDistance matrix:\n";
	/*
	 for (unsigned i=0; i<nodes.size(); i++) {
	 cout <<nodes[i]->getName() <<" ";
	 }*/

//  vector<Node*>::iterator it = nodes.begin();
//  for (it=nodes.begin(); it!=nodes.end(); it++)
//    cout <<(*it)->getName() <<" ";
//  cout <<"\n";
}

void GraphApp::compute_closeness_centrality() {
	cout << "Compute closeness centrality\n";
	this->initializeGraph();
	MyMatrix m = g->generate_distance_matrix();
	MyMatrix::iterator it = m.begin();
	MapType::iterator d;
	unsigned total_distance;

	for (it = m.begin(); it != m.end(); it++) {
		cout << (*it).first << ": ";
		total_distance = 0;
		for (d = (*it).second.begin(); d != (*it).second.end(); d++)
			//cout <<(*d).first <<"->" <<(*d).second <<" ";
			total_distance += (*d).second;
		cout << (float) 1 / total_distance << ": " << total_distance << endl;
	}
}

void GraphApp::compute_betweenness_centrality() {
	cout << "Compute betweenness centrality\n";
	this->initializeGraph();
	float b = this->g->brandes_betweenness_centrality(true, true);
	cout << b << "\n";
}

void GraphApp::compare_degree() {
//  Graph* g1 = new Graph(FPCBMC_FILENAME,Graph::REC);
//  Graph* g2 = new Graph(MUSCLE_FILENAME,Graph::REC);
//  Graph* g3 = new Graph(ARC_FILENAME,Graph::REC);
//  Graph* g4 = new Graph(ARC2_FILENAME,Graph::REC);

//  Graph* g1 = new Graph(FPCBMC_META,Graph::N2N);
//  Graph* g2 = new Graph(MUSCLE_META,Graph::N2N);
//  Graph* g3 = new Graph(ARC_META,Graph::N2N);
//  Graph* g4 = new Graph(ARC2_META,Graph::N2N);

	Graph* g1 = new Graph(FPCBMC_REAC, Graph::N2N);
	Graph* g2 = new Graph(MUSCLE_REAC, Graph::N2N);
	Graph* g3 = new Graph(ARC_REAC, Graph::N2N);
	Graph* g4 = new Graph(ARC2_REAC, Graph::N2N);

	// Tinh va so sanh cac rev vs. irrev; int vs. ext
	struct unit {
			int soluong;
			int tongbac;
	};

	struct unit fpcbmc[4];
	struct unit muscle[4];
	struct unit aracell[4];
	struct unit aracell2[4];

	for (unsigned i = 0; i < 4; i++) {
		fpcbmc[i].soluong = 0;
		fpcbmc[i].tongbac = 0;
		muscle[i].soluong = 0;
		muscle[i].tongbac = 0;
		aracell[i].soluong = 0;
		aracell[i].tongbac = 0;
		aracell2[i].soluong = 0;
		aracell2[i].tongbac = 0;
	}

	g1->degrees();
	g1->classifyNodes(FPCBMC_FILENAME);

	Table* tab = new Table(4, "Degree");

	tab->set_column_name(0, "FPCBMC");
	tab->set_column_name(1, "Muscle");
	tab->set_column_name(2, "Aracell");
	tab->set_column_name(3, "Aracell2");

	vector<Node*> nodes = g1->sortNodesByName();
	for (unsigned i = 0; i < nodes.size(); i++) {
		Record* r = new Record(nodes[i]->getName(), nodes[i]->degree(), 0.0,
		        0.0, 0.0);
		char type = MoleculeTypes[nodes[i]->getType()];
		switch (type) {
			case 1: //Molecule::REVENZ:
				fpcbmc[0].soluong += 1;
				fpcbmc[0].tongbac += nodes[i]->degree();
				break;
			case 2: //Molecule::IRRENZ:
				fpcbmc[1].soluong += 1;
				fpcbmc[1].tongbac += nodes[i]->degree();
				break;
			case 3: //Molecule::INTMET:
				fpcbmc[2].soluong += 1;
				fpcbmc[2].tongbac += nodes[i]->degree();
				break;
			case 4: //Molecule::EXTMET:
				fpcbmc[3].soluong += 1;
				fpcbmc[3].tongbac += nodes[i]->degree();
				break;
		}
		tab->add_record(r);
	}

	g2->degrees();
	nodes = g2->sortNodesByDegree();
	g2->classifyNodes(MUSCLE_FILENAME);
	for (unsigned i = 0; i < nodes.size(); i++) {
		Record* r;
		r = tab->find(nodes[i]->getName());
		if (r) {
			// Update
			r->update(1, nodes[i]->degree(), Record::REP);
		}
		else {
			r = new Record(nodes[i]->getName(), 0.0, nodes[i]->degree(), 0.0,
			        0.0);
			tab->add_record(r);
		}
		char type = MoleculeTypes[nodes[i]->getType()];
		switch (type) {
			case 1: //Molecule::REVENZ:
				muscle[0].soluong += 1;
				muscle[0].tongbac += nodes[i]->degree();
				break;
			case 2: //Molecule::IRRENZ:
				muscle[1].soluong += 1;
				muscle[1].tongbac += nodes[i]->degree();
				break;
			case 3: //Molecule::INTMET:
				muscle[2].soluong += 1;
				muscle[2].tongbac += nodes[i]->degree();
				break;
			case 4: //Molecule::EXTMET:
				muscle[3].soluong += 1;
				muscle[3].tongbac += nodes[i]->degree();
				break;
		}
	}

	g3->degrees();
	nodes = g3->sortNodesByDegree();
	g3->classifyNodes(ARC_FILENAME);
	for (unsigned i = 0; i < nodes.size(); i++) {
		Record* r;
		r = tab->find(nodes[i]->getName());
		if (r) {
			// Update
			r->update(2, nodes[i]->degree(), Record::REP);
		}
		else {
			r = new Record(nodes[i]->getName(), 0.0, 0.0, nodes[i]->degree(),
			        0.0);
			tab->add_record(r);
		}
		char type = MoleculeTypes[nodes[i]->getType()];
		switch (type) {
			case 1: //Molecule::REVENZ:
				aracell[0].soluong += 1;
				aracell[0].tongbac += nodes[i]->degree();
				break;
			case 2: //Molecule::IRRENZ:
				aracell[1].soluong += 1;
				aracell[1].tongbac += nodes[i]->degree();
				break;
			case 3: //Molecule::INTMET:
				aracell[2].soluong += 1;
				aracell[2].tongbac += nodes[i]->degree();
				break;
			case 4: //Molecule::EXTMET:
				aracell[3].soluong += 1;
				aracell[3].tongbac += nodes[i]->degree();
				break;
		}
	}

	g4->degrees();
	nodes = g4->sortNodesByDegree();
	g4->classifyNodes(ARC2_FILENAME);
	for (unsigned i = 0; i < nodes.size(); i++) {
		Record* r;
		r = tab->find(nodes[i]->getName());
		if (r) {
			// Update
			r->update(3, nodes[i]->degree(), Record::REP);
		}
		else {
			r = new Record(nodes[i]->getName(), 0.0, 0.0, 0.0,
			        nodes[i]->degree());
			tab->add_record(r);
		}
		char type = MoleculeTypes[nodes[i]->getType()];
		switch (type) {
			case 1: //Molecule::REVENZ:
				aracell2[0].soluong += 1;
				aracell2[0].tongbac += nodes[i]->degree();
				break;
			case 2: //Molecule::IRRENZ:
				aracell2[1].soluong += 1;
				aracell2[1].tongbac += nodes[i]->degree();
				break;
			case 3: //Molecule::INTMET:
				aracell2[2].soluong += 1;
				aracell2[2].tongbac += nodes[i]->degree();
				break;
			case 4: //Molecule::EXTMET:
				aracell2[3].soluong += 1;
				aracell2[3].tongbac += nodes[i]->degree();
				break;
		}
	}
	tab->display();

	cout
	        << "\n\n=======Reaction==============================================\n";
	cout << "FPCBMC ==>  Rev: " << fpcbmc[0].tongbac / fpcbmc[0].soluong
	        << ", Irrev: " << fpcbmc[1].tongbac / fpcbmc[1].soluong << endl;
	cout << "Muscle ==>  Rev: " << muscle[0].tongbac / muscle[0].soluong
	        << ", Irrev: " << muscle[1].tongbac / muscle[1].soluong << endl;
	cout << "Aracell ==>  Rev: " << aracell[0].tongbac / aracell[0].soluong
	        << ", Irrev: " << aracell[1].tongbac / aracell[1].soluong << endl;
	cout << "Aracell2 ==>  Rev: " << aracell2[0].tongbac / aracell2[0].soluong
	        << ", Irrev: " << aracell2[1].tongbac / aracell2[1].soluong << endl;
	cout
	        << "\n\n=======Metabolite==============================================\n";
	cout << "FPCBMC ==>  Int: " << (float) fpcbmc[2].tongbac / fpcbmc[2].soluong
	        << ", Ext: " << (float) fpcbmc[3].tongbac / fpcbmc[3].soluong
	        << endl;
	cout << "Muscle ==>  Int: " << (float) muscle[2].tongbac / muscle[2].soluong
	        << ", Ext: " << (float) muscle[3].tongbac / muscle[3].soluong
	        << endl;
	cout << "Aracell ==>  Int: "
	        << (float) aracell[2].tongbac / aracell[2].soluong << ", Ext: "
	        << (float) aracell[3].tongbac / aracell[3].soluong << endl;
	cout << "Aracell2 ==>  Int: "
	        << (float) aracell2[2].tongbac / aracell2[2].soluong << ", Ext: "
	        << (float) aracell2[3].tongbac / aracell2[3].soluong << endl;
}

void GraphApp::build_reaction_graph_from_stoichiometric_matrix(
        const std::string & filename) {
	cout << "Build a reaction graph from its stoichiometric matrix file " <<endl
	        << filename << endl
	        <<"Format:" <<endl
	        <<"The first line: rows columns" <<endl
	        <<"The second line: the list of reactions" <<endl
	        <<"The following rows  represent feasible routes which associated between the metabolite in the rows and the reactions." <<endl;

	ifstream fdata;
	fdata.open(filename.c_str());

	if (fdata.is_open() && !fdata.eof() ) {
		unsigned nbRows = 0, nbCols = 0;
		char so[4];

		fdata >> so;
		nbRows = atoi(so);
		fdata >> so;
		nbCols = atoi(so);
		string reactions[nbCols];
		for (unsigned i = 0; i < nbCols; i++)
			fdata >>reactions[i];
		int* stoichiometricMatrix = new int[nbRows*nbCols];
		for (unsigned  i = 0; i < nbRows; i++) {
			for (unsigned  j = 0; j < nbCols; j++) {
				fdata >> stoichiometricMatrix[i*nbCols + j];
			}
		}
		vector< pair<unsigned,unsigned> > temp_edges;
		for (unsigned  i = 0; i < nbRows; i++) {
			for (unsigned  j = 0; j < nbCols-1; j++) {
				for (unsigned k = j+1; k < nbCols; k++) {
					if (stoichiometricMatrix[i*nbCols + j] && stoichiometricMatrix[i*nbCols + k])
						temp_edges.push_back(make_pair(j,k));
				}
			}
		}

		for (vector< pair<unsigned,unsigned> >::iterator it = temp_edges.begin(); it != temp_edges.end(); ++it) {
			g->addEdge(reactions[(*it).first],reactions[(*it).second]);
		}
	}
	//g->traverse(1);
	unsigned int* degs = g->degrees();
		cout <<degs <<endl;
		for (unsigned int i=0; i<g->getNodes().size(); i++)
			cout<<degs[i] <<" ";
		cout <<endl <<degs <<endl;
	fdata.close();
}

void GraphApp::build_stoichiometric_matrix(int m) {
	// Initialize the matrix
	double **stoichiometric_matrix;
	int ROWS, COLS;
	string from_file;

	switch (m) {
		case 1:
			ROWS = 55;
			COLS = 78;
			from_file = PATH + "stoichiometric-matrix-fpcbmc";
			break;
		case 2:
			ROWS = 49;
			COLS = 92;
			from_file = PATH + "stoichiometric-matrix-aracell";
			break;
		case 3:
			ROWS = 50;
			COLS = 89;
			from_file = PATH + "stoichiometric-matrix-aracell2";
			break;
		default:
			ROWS = 31;
			COLS = 37;
			from_file = PATH + "stoichiometric-matrix-muscle";
			break;
	}

	// Allocate memory
	stoichiometric_matrix = new double*[ROWS];
	for (int i = 0; i < ROWS; ++i)
		stoichiometric_matrix[i] = new double[COLS];

	// Assign values
	// Scan every line tokenizer

	string line, line1;
	vector<string> stoi_matrix;
	vector<string> col_names;
	vector<string> row_names;
	vector<string> temp;
	ifstream myfile(from_file.c_str());

	if (myfile.is_open()) {
		getline(myfile, line); // -ENZREV
		getline(myfile, line1);

		getline(myfile, line); // Empty line
		getline(myfile, line); // -ENZIRREV
		getline(myfile, line);
		col_names = tokenizer(line);
		temp = tokenizer(line1);
		for (unsigned i = 0; i < temp.size(); i++)
			col_names.push_back(temp[i]);
		getline(myfile, line); // Empty line
		getline(myfile, line); // -METINT
		getline(myfile, line);
		row_names = tokenizer(line);
		line = trim(line);
		// Ignore some lines that are the beginning of the data file
		while (line.compare("START"))
			getline(myfile, line);

		// Read all the remaining lines
		while (myfile.good()) {
			getline(myfile, line);
			line = trim(line);
			stoi_matrix.push_back(line);
		}
		stoi_matrix.pop_back();
		myfile.close();
	}
	else
		cout << "Unable to open file\n";

	// Dua tung gia tri vao matrix
	//vector<string>::iterator it;
	vector<string> values;
	int tmp = 0;
	//for (it = v.begin(); it != v.end(); it++) {
	for (unsigned t = 0; t < stoi_matrix.size(); t++) {
		//values = tokenizer(*it);
		values = tokenizer(stoi_matrix[t]);
		for (unsigned i = 0; i < values.size(); i++) {
			tmp = atoi(values[i].c_str());
			stoichiometric_matrix[t][i] = tmp;
		}
	}

	// Build a reaction network
	/*
	 for (int i=0; i<ROWS; i++) {
	 for (int j=0; j<COLS-1; j++)
	 for (int k=j+1; k<COLS; k++)
	 if (stoichiometric_matrix[i][j] && stoichiometric_matrix[i][k])
	 cout <<col_names[j] <<" " <<col_names[k] <<"\n";
	 }
	 */

	// Build a metabolite network
	for (int i = 0; i < COLS; i++) {
		for (int j = 0; j < ROWS - 1; j++)
			for (int k = j + 1; k < ROWS; k++)
				if (stoichiometric_matrix[j][i] && stoichiometric_matrix[k][i])
					cout << row_names[j] << " " << row_names[k] << "\n";
	}
	// De-Allocate memory to prevent memory leak
	for (int i = 0; i < ROWS; ++i)
		delete[] stoichiometric_matrix[i];
	delete[] stoichiometric_matrix;
}

/*========================================
 Check two edges is in the same source
 and target node with different directions
 ========================================*/
void GraphApp::check_duplicated_edges() {
	this->initializeGraph();
	vector<Edge*> edges = this->g->getEdges();
	int kq = 0;

	for (unsigned i = 0; i < edges.size() - 1; i++)
		for (unsigned j = i + 1; j < edges.size(); j++) {
			string org1 = edges[i]->getOrgNode()->getName();
			string dst1 = edges[i]->getDstNode()->getName();
			string org2 = edges[j]->getOrgNode()->getName();
			string dst2 = edges[j]->getDstNode()->getName();
			if (((strcasecmp(org1.c_str(), dst2.c_str()) == 0
			        && strcasecmp(org2.c_str(), dst1.c_str()) == 0))
			        || ((strcasecmp(org1.c_str(), org2.c_str()) == 0
			                && strcasecmp(dst1.c_str(), dst2.c_str()) == 0))) {
				//cout <<i+1 <<" " <<edges[i]->getOrgNode()->getName() <<" " <<edges[i]->getDstNode()->getName() <<endl;
				cout << j + 1 << " " << edges[j]->getOrgNode()->getName() << " "
				        << edges[j]->getDstNode()->getName() << endl;
				kq++;
			}
		}
	cout << "Found: " << kq / 2 << " duplicated edge(s)" << endl;
}

void GraphApp::check_duplicated_edges(string filename) {
	cout << filename << endl;
	g = new Graph(filename, Graph::N2N);
	set<unsigned> duplicatedEdgeIndices = g->hasDuplicatedEdges();
	cout << "Found: " << duplicatedEdgeIndices.size() << " duplicated pairs of edge(s)" << endl;
	cout <<"The edges before removing the duplicated edges:" <<endl;
	//g->traverse(0);
	vector<Edge*> edges = g->getEdges();
	set<unsigned>::iterator it;
	for (it=duplicatedEdgeIndices.begin();it != duplicatedEdgeIndices.end(); ++it)
		g->removeEdge(edges[*it]);
	cout <<"The edges after removing the duplicated edges::" <<endl;
	g->traverse(0);
}

void GraphApp::remove_duplicated_edges() {
	set<unsigned> duplicatedEdgeIndices = g->hasDuplicatedEdges();
	if (duplicatedEdgeIndices.size()) {
		vector<Edge*> edges = g->getEdges();
		set<unsigned>::iterator it;
		for (it=duplicatedEdgeIndices.begin();it != duplicatedEdgeIndices.end(); ++it)
			g->removeEdge(edges[*it]);
		cout <<"The edges after removing the duplicated edges::" <<endl;
		g->traverse(1);
	}
}

// Create a graph by loading metatool file
void GraphApp::create_graph_from_data_file(string file_name) {
	Graph* g = new Graph(file_name, Graph::REC);

	g->traverse(0);

	cout << "\nSo dinh: " << g->numberVertices() << endl;
	cout << "So canh: " << g->numberEdges() << endl;
	g->classifyNodes(file_name);
	// Display the level of the nodes/vertices
	unsigned* degs = g->degrees();
	int i = 0;
	while (*degs) {
		cout << *(++degs) << "  ";
		++i;
	}
	cout << "\n";
}

// Create a simple graph via adding a few nodes
void GraphApp::create_graph_manually() {
	//Graph* g = new Graph();
	Node* node1 = new Node("1");
	Node* node2 = new Node("2");
	Node* node3 = new Node("3");
	Node* node4 = new Node("4");
	Node* node5 = new Node("5");

	node1->setType(Molecule::EXTMET);
	node2->setType(Molecule::INTMET);
	node3->setType(Molecule::EXTMET);
	node4->setType(Molecule::INTMET);
	node5->setType(Molecule::INTMET);

	node1->addAdjNode(node2, 10);
	node1->addAdjNode(node4, 20);

	node3->addAdjNode(node5, 10);
	node5->addAdjNode(node4, 20);
	node1->addAdjNode(node5, 10);
	g->addNode(node1);
	g->addNode(node2);
	g->addNode(node3);
	g->addNode(node4);
	g->addNode(node5);
	g->traverse(0);
	cout << "\nSo dinh: " << g->numberVertices() << "\nSo canh: "
	        << g->numberEdges() << endl;
	cout << "Tim canh e(1,2)\n";
	Edge* e = g->findEdgeByNames("1", "2");
	if (e)
		e->display();
	else
		cout << "\nNot found. \n";

	cout << "\nScan on graph successfully.\n";
}

// Create a simple graph from two reactions
void GraphApp::create_simple_graph() {
	cout <<"1. Creating a graph from a reaction..." <<endl;
	string reaction = "Vala : pyr + glu = ala + aKG .";
	Reaction* r = new Reaction();
	r->createReaction(reaction);
	r->display();
	g->createFromReaction(r);
	g->traverse(2);

	string react2 = "Vasp : OAA + glu = asp + aKG .";
	cout <<"2. Adding edges to the graph from another reaction..." <<endl;
	cout <<react2 << endl;
	g->createFromReaction(react2);
	g->traverse(2);

	cout <<"3. Finding the edge..." <<endl;
	Edge* e = g->findEdgeByNames("asp","Vasp");
	if (e) {
		cout <<"The edge found at: ";
		e->display();
		cout <<&e <<endl;
	}
	else
		cout <<"Edge not found." <<endl;

	string s1 = "Vasp";
	string s2 = "H2O";
	cout <<"4. Adding a new edge..." <<s1 <<" " <<s2 <<endl;
	g->addEdge(s1,s2);
	g->traverse(2);

	string rr = "aKG";
	cout << "5. Remove the node which  name is " <<rr << endl;
	g->removeNode(rr);
	g->traverse(2);
}

void GraphApp::test_set() {
	// More details from cplusplus
	set<string> myset;
	set<string>::iterator it;
	for (int i = 1; i <= 5; i++)
		myset.insert("Tung");
	myset.insert("Hue");
	myset.insert("Khang");

	cout << "myset contains:";
	for (it = myset.begin(); it != myset.end(); it++)
		cout << " " << *it;
	cout << endl;
}

void GraphApp::test_classify_nodes() {
	string filename = "reversible-muscle.dat";
	set<string> v = load_reversible_enzyme(filename);
	set<string>::iterator it = v.find("R11");
	if (it != v.end())
		cout << "Da tim thay " << *it << endl;
	else
		cout << "khong tim thay\n";
	for (it = v.begin(); it != v.end(); it++)
		cout << *it << endl;
}

void GraphApp::test_sort() {
	Graph* g = new Graph(MUSCLE_FILENAME, Graph::REC);
	vector<Node*> nodes = g->sortNodesByName();
	for (unsigned i = 0; i < nodes.size(); i++)
		cout << nodes[i]->getName() << endl;
}

void GraphApp::test_map_3() {
	typedef std::map<char, int> MapType;
	MapType my_map;

	// insert elements using insert function
	my_map.insert(std::pair<char, int>('a', 1));
	my_map.insert(std::pair<char, int>('b', 2));
	my_map.insert(std::pair<char, int>('c', 3));
	my_map.insert(MapType::value_type('d', 4)); // all standard containers provide this typedef
	my_map.insert(std::make_pair('e', 5)); // can also use the utility function make_pair

	//map keys are sorted automatically from lower to higher.
	//So, my_map.begin() points to the lowest key value not the key which was inserted first.
	MapType::iterator iter = my_map.begin();

	// erase the first element using the erase function
	my_map.erase(iter);

	// output the size of the map
	std::cout << "Size of my_map: " << my_map.size() << '\n';

	std::cout << "Enter a key to search for: ";
	char c;
	std::cin >> c;

	// find will return an iterator to the matching element if it is found
	// or to the end of the map if the key is not found
	iter = my_map.find(c);
	if (iter != my_map.end())
		std::cout << "Value is: " << iter->second << '\n';
	else
		std::cout << "Key is not in my_map" << '\n';
	cout << "---------------------------------\n";

	iter = my_map.begin();
	while (iter != my_map.end()) {
		std::cout << "Value is: " << iter->second << '\n';

	}

	// clear the entries in the map
	my_map.clear();
}

void GraphApp::test_map_2() {
	typedef std::map<std::string, int> MapType;
	MapType data;

	// let's declare some initial values to this map
	data["Bobs score"] = 10;
	data["Martys score"] = 15;
	data["Mehmets score"] = 34;
	data["Rockys score"] = 22;
	//data["Rockys score"] = 23;  /*overwrites the 22 as keys are unique */

	// Iterate over the map and print out all key/value pairs.
	// Using a const_iterator since we are not going to change the values.
	MapType::const_iterator end = data.end();
	for (MapType::const_iterator it = data.begin(); it != end; ++it) {
		std::cout << "Who(key = first): " << it->first;
		std::cout << " Score(value = second): " << it->second << '\n';
	}
}

void GraphApp::test_map() {
	std::map<std::string, int> wordcounts;
	std::string s;

	while (std::cin >> s && s != "end")
		++wordcounts[s];

	while (std::cin >> s && s != "end")
		std::cout << s << ' ' << wordcounts[s] << '\n';
}

void GraphApp::test_hash_map() {
	typedef map<int, double> Map;
	typedef Map::value_type value_type;
	typedef Map::iterator iterator;

	Map map;
	map.insert(value_type(8, 8.888));
	map.insert(value_type(1, 1.111));
	map.insert(value_type(12, 12.12));
	map.insert(value_type(3, 3.3));
	map.insert(value_type(122, 122.122));

	std::cout << "\nSize is: " << map.size();
	std::cout << "\nElements are:";
	for (iterator it = map.begin(); it != map.end(); ++it) {
		std::cout << "\n\tKey = " << it->first << "   Value = " << it->second;
	}
	cout << "\n";
}

void GraphApp::test1() {
	char str[2000];
	fstream file_op("fpcBMC.dat", ios::in);
	while (file_op >> str)
		cout << str << endl;
	cout << endl;
	file_op.close();
	string s = "abc edc";
	vector<string> v = tokenizer(s);
	for (unsigned i = 0; i < v.size(); i++)
		cout << v[i] << endl;

	string s1 = "Tung";
	string s2 = "TUNG";

	if (strcasecmp(s1.c_str(), s2.c_str()))
		cout << s1 << " va " << s2 << ": Khong Giong nhau";
	else
		cout << s1 << " va " << s2 << ": Giong nhau";
	cout << endl;
}

void GraphApp::test2() {
	string str;
	fstream file("mito-muscle-modif.dat", ios::in);
	while (!file.eof()) {
		getline(file, str);
		cout << str;
	}
	file.close();

	cout << endl;
}

void GraphApp::test3() {
	// first lets output to a file
	ofstream fout("sample.txt");
	fout << "test test test" << endl;
	fout.close();
	char str[20];
	//read in the file
	ifstream fin("sample.txt");
	fin >> str;
	fin.close();
	//display sample.txt contents
	cout << "data read from file: " << str << endl;
}

void GraphApp::test4() {
	string line;
	ifstream myfile(MUSCLE_FILENAME.c_str());
	vector<string> reactions;

	if (myfile.is_open()) {
		getline(myfile, line);
		// Ignore some lines that are the beginning of the data file
		while (line.compare("-CAT"))
			getline(myfile, line);

		// Read all the remaining lines
		while (myfile.good()) {
			getline(myfile, line);
			reactions.push_back(line);
			cout << line << endl;
		}
		myfile.close();
		cout << "The number of reactions is: " << reactions.size() - 1 << endl;
	}

	else
		cout << "Unable to open file";
}

void GraphApp::test5() {
	// Parse components of a reaction: reaction name, left side and right side
	string reaction = "Vkgdh : aKG = CO2 + NADH + succ .";
	// "  NRJ1b   : 2 NADPH = 3 ATP .";
	size_t found_dc = reaction.find(":"); // Position of double colon
	size_t found_eq = reaction.find("="); // Position of equal

	string r_name = reaction.substr(0, found_dc - 1);
	// Get the compounds on the left side of the reaction
	string r_reactants = reaction.substr(found_dc + 1, found_eq - found_dc - 1);
	// Get the compounds on the right side of the reaction
	string r_products = reaction.substr(found_eq + 1,
	        reaction.length() - found_eq - 2);
	cout << "Ten phan ung: " << r_name << endl;
	cout << "Ve trai: " << r_reactants << endl;
	cout << "Ve phai: " << r_products << endl;
	cout << "[" << r_products << "]" << endl;
	cout << "[" << trim(r_products) << "]" << endl;
}

void GraphApp::compute_degree() {
	unsigned int* degs = g->degrees();
	for (unsigned int i=0; i<g->getNodes().size(); i++)
		cout<<degs[i] <<" ";
}

void GraphApp::test7() {
	char str[] = " CO2 + 3NADH + 2 succ";
	//string str =" CO2 + 3NADH + 2 succ";
	char* pch;
	cout << "Splitting string \"" << str << "\" into tokens:\n";
	pch = strtok(str, " ,.-+");
	while (pch != NULL) {
		cout << pch << "\n";
		pch = strtok(NULL, " ,.-+");
	}
}

void GraphApp::test8() {
	string str = "3NaOH";

	if (isNumber(str))
		cout << "yes!" << endl;
	else
		cout << "no" << endl;

	//string r = " CO2 + 3NADH + 2 succ";
	string r = "8 AccoA_p + 7 ATP + 14 NADPH";
	cout << r << endl;
	vector<string> comps = normalize_reaction(r);
	cout << comps.size() << endl;
	for (unsigned i = 0; i < comps.size(); i++)
		cout << comps[i] << " ";
	cout << endl;
}

void GraphApp::test9() {
	string file_name = "aracell2.dat"; //"aracell.dat";//"mito-muscle-modif.dat"; //"fpcBMC.dat"; //
	Reaction* r;
	vector<string> reactions = load_reactions(file_name);
	for (unsigned i = 0; i < reactions.size() - 1; i++) {
		r = new Reaction();
		r->createReaction(reactions[i]);
		//r->display();
		r->printEdges();
	}
	cout << reactions.size() - 1 << endl;
}

void GraphApp::test10() {
	//string reaction = "Vkgdh : aKG = CO2 + NADH + succ .";
	/*string reaction = "NRJ1b   : 2 NADPH = 3 ATP .";
	Reaction* r = new Reaction();
	r->createReaction(reaction);
	r->display();*/
	unsigned int* degs = g->degrees();
	vector<Node*> nodes = g->getNodes();
	for (unsigned int i = 0; i < nodes.size(); i++)
		cout << nodes[i]->getName() <<"\t" <<degs[i] <<endl;
	cout << endl << degs << endl;
	string line = "R6i : Pyr + CO2 + ATP = OAA + Pi + ADP .";
	vector<string> v = tokenizer(line);
	cout <<v.size() <<endl;
	for (unsigned i=0; i< v.size(); i++)
		cout <<v[i] <<" ";

	int ra[ ] = {1, 2, 4, 6, 8, 10, 12};
	int rb[ ] = {1, 3, 6, 9, 12, 15, 18};
	vector<int> a(ra, ra+7);
	vector<int> b(rb, rb+7);
	vector<int> c;

	set_intersection(a.begin(), a.end(),
			b.begin(), b.end(),
			back_inserter(c));

    for(unsigned i=0; i<c.size(); i++)
		cout << c[i] << " ";
	cout << endl;

	vector<int> v1 = {1,3,4,5};
	vector<int> v2 = {2,4,5};
	vector<int> v3;
	set_intersection(v1.begin(),v1.end(),
				v2.begin(), v2.end(), back_inserter(v3));
    for(unsigned i=0; i<v3.size(); i++)
			cout << v3[i] << " ";
		cout << endl;
	if (v3.size())
		cout <<"Co phan tu chung" <<endl;
	else
		cout <<"Khong Co phan tu chung" <<endl;
	set<int> sa(ra, ra+7);
	set<int> sb(rb, rb+7);
	set<int> sc;

	set_intersection(sa.begin(), sa.end(),
			sb.begin(), sb.end(),
			inserter(sc, sc.end()));

	for(set<int>::iterator it=sc.begin(); it != sc.end(); it++)
		cout << *it << " ";
	cout << endl;
}

void GraphApp::merge2lists() {
	list<int> lst1, lst2;
	int i;

	for (i = 0; i < 10; i += 2)
		lst1.push_back(i);
	for (i = 1; i < 11; i += 2)
		lst2.push_back(i);

	list<int>::iterator p = lst1.begin();
	while (p != lst1.end()) {
		cout << *p << endl;
		p++;
	}

	p = lst2.begin();
	while (p != lst2.end()) {
		cout << *p << endl;
		p++;
	}

	lst1.merge(lst2);
	if (lst2.empty())
		cout << "lst2 is now empty\n";

	cout << "Contents of lst1 after merge:\n";
	p = lst1.begin();
	while (p != lst1.end()) {
		cout << *p << " ";
		p++;
	}
}

void GraphApp::test_Tableclass() {
	Table* tab = new Table(4, "Degree");

	tab->set_column_name(0, "FPCBMC");
	tab->set_column_name(1, "Muscle");
	tab->set_column_name(2, "Aracell");
	tab->set_column_name(3, "Aracell2");
	Record* r = new Record("ATP", 1.2, 1.3, 1.4, 1.5);
	tab->add_record(r);
	tab->display();
	Record* r1 = tab->find("ATP2");

	if (r1) {
		cout << "Ket qua tim ATP: ";
		r1->display();
	}
	else {
		cout << "Khong tim thay\n";
	}

	Record* r2;
	cout << &r2 << endl;
	r2 = tab->find("NADH");
	cout << &r2 << endl;
	if (r2) {
		cout << &r2 << endl;
		cout << "tim thay\n";
	}
	else {
		r2 = new Record("NADH", 1.0, 1.0, 1.0, 1.0);
		cout << &r2 << endl;
		tab->add_record(r2);
	}
	tab->find("ATP")->display();

	tab->display();

	Record* r3 = NULL;
	cout << endl << &r3 << endl << "Test noi dung khac========" << endl;
	r2->update(0, 2.3, Record::REP);
	r2->display();
}

void GraphApp::foo(int& y) { // y is now a reference
	using namespace std;
	cout << "y = " << y << endl;
	y = 6;
	cout << "y = " << y << endl;
} // y is destroyed here

void GraphApp::test_reference_parameter_in_c2plus() {
	int x = 5;
	cout << "x = " << x << endl;
	foo(x);
	cout << "x = " << x << endl;
}

void GraphApp::find_all_cutsets() {
	cout << "Find all cut sets in the graph\n";
	Graph* g = new Graph();
	Node* nA = new Node("A");
	Node* nB = new Node("B");
	Node* nC = new Node("C");
	nA->addAdjNode(nB, 0);
	nA->addAdjNode(nC, 0);
	nB->addAdjNode(nC, 0);
	g->addNode(nA);
	g->addNode(nB);
	g->addNode(nC);
	cout << "Tim cut sets giua dinh A va B:\n";

	cout << "=========== Graph g ================\n";
	g->display();
	g->findMinimalCutSet(nA, nB);
	//string filename = dd.append("sample");
	string filename = "tinyG.dat";
	//string filename = "mediumG.dat";
	Graph* g2 = new Graph(filename, Graph::N2N);
	cout << "=========== Graph g2 ================\n";
	g2->display();
	Node* s = g2->findNodeByName("0");
	Node* t = g2->findNodeByName("6");
	g2->findMinimalCutSet(s, t);
	//create_combinations(5,3);
	//vector<vector<int> > combs = combinations(5,3);
	/*vector<vector<int> >::iterator it = combs.begin();

	 for (; it != combs.end(); it++) {
	 for (unsigned i = 0; i < it->size(); i++)
	 cout <<it->operator [](i)<<", ";
	 cout <<endl;
	 }
	 */

}

void GraphApp::use_lemon() {
	cout << "Use lemon library\n";
}

void GraphApp::lp_demo() {
	// Create an instance of the default LP solver class
	// (it will represent an "empty" problem at first)
	/*Lp lp;

	 // Add two columns (variables) to the problem
	 Lp::Col x1 = lp.addCol();
	 Lp::Col x2 = lp.addCol();

	 // Add rows (constraints) to the problem
	 lp.addRow(x1 - 5 <= x2);
	 lp.addRow(0 <= 2 * x1 + x2 <= 25);

	 // Set lower and upper bounds for the columns (variables)
	 lp.colLowerBound(x1, 0);
	 lp.colUpperBound(x2, 10);

	 // Specify the objective function
	 lp.max();
	 lp.obj(5 * x1 + 3 * x2);

	 // Solve the problem using the underlying LP solver
	 lp.solve();

	 // Print the results
	 if (lp.primalType() == Lp::OPTIMAL) {
	 std::cout << "Objective function value: " << lp.primal() << std::endl;
	 std::cout << "x1 = " << lp.primal(x1) << std::endl;
	 std::cout << "x2 = " << lp.primal(x2) << std::endl;
	 } else {
	 std::cout << "Optimal solution not found." << std::endl;
	 }*/
}

/*
 void GraphApp::hao_orlin_demo(string input_file) {
 typedef int Value;
 SmartDigraph graph;
 SmartDigraph::ArcMap<int> cap1(graph), cap2(graph), cap3(graph);
 SmartDigraph::NodeMap<bool> cut(graph);

 SmartDigraph::Node s, t;

 try {
 digraphReader(graph, input_file) 		// read the directed graph into graph
 .arcMap("cap1", cap1)               // read the 'capacity' arc map into cap
 .arcMap("cap2", cap2)
 .arcMap("cap3", cap3)
 .node("source", s)                  // read 'source' node to s
 .node("target", t)                  // read 'target' node to t
 .run();
 } catch (Exception& error) { // check if there are any error
 std::cerr <<"Error: " <<error.what() <<std::endl;
 exit(1);
 }

 //std::cout << "A digraph is read from an input file." << std::endl;
 //std::cout << "Number of nodes: " << countNodes(graph) << std::endl;
 //std::cout << "Number of arcs: " << countArcs(graph) << std::endl;

 for (SmartDigraph::ArcIt n(graph); n != INVALID; ++n) {
 cout << "The edge with id = " <<graph.id(n) <<" (" <<graph.id(graph.source(n))
 <<", " <<graph.id(graph.target(n))  << ") capacity = " <<cap1[n] <<".\n";
 }

 HaoOrlin<SmartDigraph> ho(graph, cap1);
 //ho.init();
 //ho.init(s);
 //ho.calculateOut();
 //ho.calculateIn();
 //ho.run();
 ho.run(s);
 Value v = ho.minCutMap(cut);
 cout <<"Min cut: " <<v <<endl;
 string ss = "{";
 string ts = "{";
 cout <<"The source side consists of: ";
 for (SmartDigraph::NodeIt n(graph); n != INVALID; ++n) {
 if (cut[n]) {
 std::ostringstream number_str;
 number_str <<graph.id(n);
 ss += number_str.str() + ", ";
 //ss += string(itoa(graph.id(n))) + ", ";
 //ss += std::to_string(graph.id(n)) + ", ";
 }
 }
 //size_t endpos = ss.find_last_not_of(" ");
 ss = ss.substr(0, ss.length()-2);
 cout <<ss <<"}\n";

 cout <<"The target side consists of: ";
 for (SmartDigraph::NodeIt n(graph); n != INVALID; ++n) {
 if (!cut[n]) {
 std::ostringstream number_str;
 number_str <<graph.id(n);
 ts += number_str.str() + ", ";
 }
 }
 ts = ts.substr(0, ts.length()-2);
 cout <<ts <<"}\n";

 cout <<"See you soon!\n";
 }
*/
 /*
 void GraphApp::boost_demo1() {
 cout <<"Boost Demo 1\n";
 std::string hello( "Hello, world!" );
 }
 */

// A graphic of the min-cut is available at <http://www.boost.org/doc/libs/release/libs/graph/doc/stoer_wagner_imgs/stoer_wagner.cpp.gif>
void GraphApp::stoer_wagner() {
	/*struct edge_t
	 {
	 unsigned long first;
	 unsigned long second;
	 };

	 typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
	 boost::no_property, boost::property<boost::edge_weight_t, int> > undirected_graph;
	 typedef boost::graph_traits<undirected_graph>::vertex_descriptor vertex_descriptor;
	 typedef boost::property_map<undirected_graph, boost::edge_weight_t>::type weight_map_type;
	 typedef boost::property_traits<weight_map_type>::value_type weight_type;

	 // define the 16 edges of the graph. {3, 4} means an undirected edge between vertices 3 and 4.
	 edge_t edges[] = {{3, 4}, {3, 6}, {3, 5}, {0, 4}, {0, 1}, {0, 6}, {0, 7},
	 {0, 5}, {0, 2}, {4, 1}, {1, 6}, {1, 5}, {6, 7}, {7, 5}, {5, 2}, {3, 4}};

	 // for each of the 16 edges, define the associated edge weight. ws[i] is the weight for the edge
	 // that is described by edges[i].
	 weight_type ws[] = {0, 3, 1, 3, 1, 2, 6, 1, 8, 1, 1, 80, 2, 1, 1, 4};

	 // construct the graph object. 8 is the number of vertices, which are numbered from 0
	 // through 7, and 16 is the number of edges.
	 undirected_graph g(edges, edges + 16, ws, 8, 16);

	 // define a property map, `parities`, that will store a boolean value for each vertex.
	 // Vertices that have the same parity after `stoer_wagner_min_cut` runs are on the same side of the min-cut.
	 BOOST_AUTO(parities, boost::make_one_bit_color_map(num_vertices(g), get(boost::vertex_index, g)));

	 // run the Stoer-Wagner algorithm to obtain the min-cut weight. `parities` is also filled in.
	 int w = boost::stoer_wagner_min_cut(g, get(boost::edge_weight, g), boost::parity_map(parities));

	 cout << "The min-cut weight of G is " << w << ".\n" << endl;
	 assert(w == 7);

	 cout << "One set of vertices consists of:" << endl;
	 size_t i;
	 for (i = 0; i < num_vertices(g); ++i) {
	 if (get(parities, i))
	 cout << i << endl;
	 }
	 cout << endl;

	 cout << "The other set of vertices consists of:" << endl;
	 for (i = 0; i < num_vertices(g); ++i) {
	 if (!get(parities, i))
	 cout << i << endl;
	 }
	 cout << endl;*/

	//return EXIT_SUCCESS;
}
