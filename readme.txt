How to run the progam:
usage: cutset [-h][-f fname [-abcdefghijksrm]][-t fname]
	-f fname loads the metabolic graph, where the fname is the name of a METATOOL file.
	-a shows the adjacent matrix
	-d show the distance matrix
	-e shows the edges list
	-k compute vertices degree 
	-m extracts the metabolite graph: generating the edges list of this graph
	-r extracts the reaction graph: generating the edges list of this graph
	-t fname creates a reaction graph from the stoichiometric matrix contained in the fname file

----

git add <file>
git add <directory>

git add -p

git commit -a -m "Message"

git push
----
git fetch
git pull