#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <vector>
#include "bio.h"

int create_combinations(int n, int k);
std::vector<std::vector<int> > combinations(int n, int k);
/** Used for
 *  displaying tuples on screen
 */
class Record {
	private:
		std::string _name; //!< Name
		float values[4]; //!< Value
		Molecule* m; //!< The list of molecules
	public:
		/**
		 * Constructor
		 */
		Record() {
			m = NULL;
		}
		/**
		 * Constructor
		 * @param name	Name of record
		 * @param v1		1st value
		 * @param v2		2nd value
		 * @param v3		3rd value
		 * @param v4		4th	value
		 */
		Record(std::string name, float v1, float v2, float v3, float v4);
		/**
		 * Constructor
		 * @param name		Name
		 * @param values	Values
		 */
		Record(std::string name, float values[4]);
		/**
		 * Display values
		 */
		void display();
		/**
		 * Operator+
		 */
		inline Record operator+(const Record& other) {
			//this->set_name(this->get_name() + other.get_name());
			this->values[0] += other.values[0];
			this->values[1] += other.values[1];
			this->values[2] += other.values[2];
			this->values[3] += other.values[3];
			return *this;
		}
		/** Assign operator
		 *  Perform in the other record
		 */
		inline bool operator==(const Record& other) const {
			return (this->_name == other._name)
					&& (this->values[0] == other.values[0]
					                                    && this->values[1] == other.values[1]
					                                                                       && this->values[2] == other.values[2]
					                                                                                                          && this->values[3] == other.values[3]);
		}

		/**
		 * Get type
		 * @return an integer
		 */
		std::string get_type() {
			return m->getType();
		}

		/**
		 * Set type
		 * @param type an integer
		 */
		void set_type(std::string type) {
			m->setType(type);
		}

		/**
		 * Get name
		 * @return
		 */
		std::string get_name();
		/**
		 * Set name
		 * @param name
		 */
		void set_name(std::string name);
		/**
		 * Merge two records
		 * @param a 1st record
		 * @param b	2nd record
		 * @return
		 */
		Record& merge(Record& a, Record& b);

		/**
		 * Update record
		 * @param index		index
		 * @param value		value
		 * @param method	method
		 * @return
		 */
		bool update(int index, float value, const unsigned method);

		//! Declaration constants
		static const unsigned REP; //!< REP
		static const unsigned ADD; //!< ADDition
		static const unsigned SUB; //!< SUBtraction
};

/** Used for
 *  holding a list of tuples
 */
class Table {
	private:
		int columns; //!< the number of columns
		std::string column_names[5]; //!< the names of the columns
		std::string table_name; //!< the table name
		std::vector<Record*> data;
	public:
		/**
		 * Constructor
		 */
		Table() {
			columns = 0;
		}
		/**
		 * Constructor
		 * @param columns
		 * @param table_name
		 */
		Table(int columns, std::string table_name);
		/**
		 * Destroy constructor
		 */
		~Table();
		/**
		 * Get columns
		 * @return
		 */
		int get_columns();
		/**
		 * Set columns
		 * @param n
		 */
		void set_columns(int n);

		/**
		 * Get the table name
		 * @return
		 */
		std::string get_table_name();
		/**
		 * Set the table name
		 * @param table_name
		 */
		void set_table_name(std::string table_name);
		/**
		 * Add the record
		 * @param r
		 */
		void add_record(Record *r);
		/**
		 * Set the column name
		 * @param column_index
		 * @param column_name
		 */
		void set_column_name(int column_index, std::string column_name);
		/**
		 * Find an element in the table
		 * @param r_name
		 * @return
		 */
		Record* find(std::string r_name); // find records whose name is r_name

		/**
		 * Display the table
		 */
		void display();
};
#endif // UTILS_H
