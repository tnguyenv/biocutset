var searchData=
[
  ['reactant',['Reactant',['../classReactant.html#a955f62cb3d14d794996684f292f68884',1,'Reactant::Reactant(const int coef=0, const std::string &amp;name=&quot;NO_NAME&quot;, const std::string type=Molecule::REACTANT)'],['../classReactant.html#a48fdc32a87fc59feb72afb590f3f65a4',1,'Reactant::Reactant(const Reactant &amp;r)']]],
  ['reaction',['Reaction',['../classReaction.html#a4ab37bd87bb5c9672de9120ada0f833f',1,'Reaction::Reaction()'],['../classReaction.html#ac206582ecfd8d0b42eacaa6a72424768',1,'Reaction::Reaction(std::string name, std::vector&lt; Reactant &gt; reactants, std::vector&lt; Product &gt; products)'],['../classReaction.html#ab781f4eb8f5d3c0ddc8643a45b51c9b5',1,'Reaction::Reaction(Reaction &amp;r)']]],
  ['readflightschedules',['readFlightSchedules',['../classGraph.html#ad37b4ed3cb07d509555a2c43dd5a2d53',1,'Graph']]],
  ['record',['Record',['../classRecord.html#ae8ee53ffec6ff4dac9911517d47e86a5',1,'Record::Record()'],['../classRecord.html#a804615c1c304982859fce50cbd09fbdb',1,'Record::Record(std::string name, float v1, float v2, float v3, float v4)'],['../classRecord.html#a973afc013e815d46156c343f101d13c7',1,'Record::Record(std::string name, float values[4])']]],
  ['remove_5fduplicated_5fedges',['remove_duplicated_edges',['../classGraphApp.html#a6a34e82e5f19cc0b4e8220912ecc30e3',1,'GraphApp']]],
  ['removeadjnode',['removeAdjNode',['../classNode.html#a9fa579dade4aa8f3a2c4fc72709e14fd',1,'Node']]],
  ['removeedge',['removeEdge',['../classGraph.html#a0b6cce5ab957f1027049c0c4d19c5df8',1,'Graph']]],
  ['removenode',['removeNode',['../classGraph.html#a7a866e89a9b2895bfa5a816c06a8e13b',1,'Graph::removeNode(std::string &amp;nodeName)'],['../classGraph.html#a826501627365639460330c3e08217328',1,'Graph::removeNode(Node *nNode)']]]
];
