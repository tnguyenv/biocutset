#include <iostream>
#include "utils.h"

using namespace std;
const unsigned Record::REP = 1;
const unsigned Record::ADD = 2;
const unsigned Record::SUB = 3;

/*==========================*/
/*  class Record            */
/*==========================*/
Record::Record(string _name, float v1, float v2, float v3, float v4) {
  this->_name = _name;
  this->values[0] = v1;
  this->values[1] = v2;
  this->values[2] = v3;
  this->values[3] = v4;
}

Record::Record(string _name, float values[4]) {
  this->_name = _name;
  this->values[0] = values[0];
  this->values[1] = values[1];
  this->values[2] = values[2];
  this->values[3] = values[3];
}

string Record::get_name() {
  return this->_name;
}

void Record::set_name(string name) {
  this->_name = name;
}

void Record::display() {
  cout <<"\"" <<this->_name <<"\", " <<this->values[0] <<", " <<values[1] <<", " <<values[2] <<", " <<values[3] <<endl;
}

Record& Record::merge(Record &a, Record &b) {
  a.operator +(b);
  return a;
}

bool Record::update(int index, float value, const unsigned method) {
  switch (method) {
  case Record::REP:
    this->values[index] = value;
    break;
  case Record::ADD:
    this->values[index] += value;
    break;
  case Record::SUB:
    this->values[index] -= value;
    break;
  default:
    this->values[index] = value;
    break;
  }

  return true;
}

/*==========================*/
/*  class Table             */
/*==========================*/

Table::Table(int columns, string table_name) {
  this->columns = columns;
  this->table_name = table_name;
}

Table::~Table() {
  for (int i=0; i<columns; i++)
    delete this->data[i];
}

int Table::get_columns() {
  return this->columns;
}

string Table::get_table_name() {
  return this->table_name;
}

void Table::set_columns(int n) {
  this->columns = n;
}

void Table::set_column_name(int column_index, string column_name) {
  this->column_names[column_index] = column_name;
}

void Table::set_table_name(string table_name) {
  this->table_name = table_name;
}

void Table::add_record(Record *r) {
  this->data.push_back(r);
}

Record* Table::find(string r_name) {
  for (unsigned i=0; i<this->data.size(); i++)
    if (strcasecmp(data[i]->get_name().c_str(),r_name.c_str()) == 0)
      return data[i];
  return NULL;
}

void Table::display() {
  cout <<table_name <<endl <<column_names[0] <<" " <<column_names[1] <<" " <<column_names[2] <<" " <<column_names[3] <<"\n";
  vector<Record*>::iterator iter;
  for (iter = this->data.begin(); iter != this->data.end(); ++iter) {
    (*iter)->display();
  }
}

int create_combinations(int n, int k) {
	int a[k];
	int count=1;

	for(int j =0 ; j<k ; j++)
		a[j]=j;
	int i;

	do
	{
		//cout.width(3);
		cout<<count++<<" : ";
		for(int j =0 ; j<k ; j++)
			cout<<a[j]+1<<",";
		cout<<endl;
		i=k-1;
		while (a[i]==n-k+i)
		{
			i--;
			if (i<0) break;
		}
		if (i<0) break;
		a[i]+=1;

		for(int j = i+1 ; j<k ; j++)
			a[j]=a[i]+j-i;
	}while(true);
	return 1;
}

vector<vector<int> > combinations(int n, int k) {
	vector<vector<int> > results;
	int a[k];
	//int count=1;

	for(int j = 0 ; j<k ; j++) {
		a[j]=j;
		//acomb.push_back(a[j]);
	}
	int i;

	do {
		vector<int> acomb;
		//cout.width(3);
		//cout <<count++ <<" : ";
		for(int j =0 ; j<k ; j++) {
			//cout <<a[j]+1 <<",";
			acomb.push_back(a[j] + 1);
		}
		//cout<<endl;
		results.push_back(acomb);
		i=k-1;
		while (a[i]==n-k+i)
		{
			i--;
			if (i<0) break;
		}
		if (i<0) break;
		a[i] += 1;

		for (int j = i+1 ; j<k ; j++)
			a[j]=a[i]+j-i;
	} while(true);

	return results;
}

/* Function prints Intersection of arr1[] and arr2[]
   m is the number of elements in arr1[]
   n is the number of elements in arr2[]
inline int isCommon(vector<string> v1, vector<string> v2, int m, int n) {
	int i = 0, j = 0;
	vector<string> common;
	while(i < m && j < n)
	{
		if(v1[i] == v2j])
			i++;
		else if(v2[j] < v1[i])
			j++;
		else  if arr1[i] == arr2[j]
		{
			common.insert(v2[j++])
			i++;
		}
	}

	return common.size();
}*/

