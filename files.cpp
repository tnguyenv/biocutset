#include <iostream>
#include <fstream>
#include <vector>
#include <cctype>
#include <string.h>
#include <stdlib.h> // atoi
#include <sstream> // ostringstream
#include <iomanip>

#include "files.h"

using namespace std;

string& trim(string &str) {
	int i, j, start, end;
	//ltrim
	for (i = 0; (str[i] != 0 && str[i] <= 32);)
		i++;
	start = i;

	//rtrim
	for (i = 0, j = 0; str[i] != 0; i++)
		j = ((str[i] <= 32) ? j + 1 : 0);
	end = i - j;
	str = str.substr(start, end - start);
	return str;
}

string& ltrim(string &str) {
	int i, start, end;

	for (i = 0; (str[i] != 0 && str[i] <= 32);)
		i++;
	start = i;
	end = str.length() - start;

	str = str.substr(start, end);
	return str;
}

string& rtrim(string &str) {
	int i, j, start = 0, end;

	for (i = 0, j = 0; str[i] != 0; i++)
		j = ((str[i] <= 32) ? j + 1 : 0);
	end = i - j;

	str = str.substr(start, end);
	return str;
}

bool isNumber(const string& s) {
	for (unsigned i = 0; i < s.length(); i++) {
		if (!isdigit(s[i]))
			return false;
	}

	return true;
}

vector<string> tokenizer(string& st) {
	vector<string> results;
	const char* delimiter = ":= ,.+";
	char* pch;
	char* s = new char[st.size() + 1];
	strncpy(s, st.c_str(), st.size() + 1);
	pch = strtok(s, delimiter);
	while (pch != NULL) {
		results.push_back(pch);
		pch = strtok(NULL, delimiter);
	}
	return results;
}

vector<string> load_edges(string &from_file) {
	ifstream myfile(from_file.c_str());
	vector<string> edges;
	string line;

	if (myfile.is_open()) {
		// Read all the remaining lines
		while (myfile.good()) {
			getline(myfile, line);
			line = trim(line);
			edges.push_back(line);
		}
		myfile.close();
	}
	else
		cout << "Unable to open file";

	return edges;
}

vector<string> load_reactions(string& from_file) {
	ifstream myfile(from_file.c_str());
	vector<string> reactions;
	string line;

	if (myfile.is_open()) {
		getline(myfile, line);
		// Ignore some lines that are the beginning of the data file
		line = trim(line);
		while (line.compare("-CAT")) {
			getline(myfile, line);
			line = trim(line);
		}

		// Read all the remaining lines
		while (myfile.good()) {
			getline(myfile, line);
			line = trim(line);
			if (line.length())
				reactions.push_back(line);
		}
		myfile.close();
	}
	else
		cout << "Unable to open file";

	return reactions;
}

vector<string> normalize_reaction(string& s) {
	vector<string> compounds;
	vector<string> results;
	results.push_back("1"); // Phan tu cam canh
	// Trim all
	s = trim(s);
	// Split the reaction into tokens
	compounds = tokenizer(s);

	// Check whether token is a number or string to add the corresponding coefficient
	// Ex: CO2 + 3NADH + 2 succ ==> tokens = [CO2,3NADH,2,succ] ==>normalize ==> [1,CO2,3,NADH,2,succ]
	for (unsigned i = 0; i < compounds.size(); i++)
		// Dang so: 2,3,...
		if (isNumber(compounds[i]))
			results.push_back(compounds[i]);
		// Khong phai la so, la dang chu: 3NADH hoac CO2, luu y: 3PG la: 3-phosphoglycerate, khi viet lien hieu la 1 substance
		//else {
		// dang: 3NADH
		//if (isdigit(compounds[i][0])) {
		// get coefficient
		//  int Number = atoi(compounds[i].c_str());;
		//  string s = static_cast<ostringstream*>(&(ostringstream() << Number))->str();
		//  results.push_back(s);
		// lay phan con lai bo vao vector results, tam lay toan bo
		//  results.push_back(compounds[i].substr(s.length()));
		// }
		// dang binh thuong nhu CO2
		else {
			// Kiem tra truoc do no co he so chua, neu co thi
			if (!isNumber(results.back())) // neu truoc do la so
				results.push_back("1"); // Gan he so ngam dinh la 1
			results.push_back(compounds[i]);
		}
	//}
	// Remove phan tu cam canh
	if (isNumber(results.at(0)) && isNumber(results.at(1)))
		results.erase(results.begin());
	return results;
}

set<string> load_reversible_enzyme(string &filename) {
	ifstream myfile(filename.c_str());
	set<string> enzymes;
	string line;

	if (myfile.is_open()) {
		getline(myfile, line);
		// Ignore some lines that are the beginning of the data file
		line = trim(line);
		while (line.compare("-ENZREV")) {
			getline(myfile, line);
			line = trim(line);
		}

		// Read all the remaining lines
		while (myfile.good()) {
			getline(myfile, line);
			line = trim(line);
			vector<string> v = tokenizer(line);
			for (unsigned j = 0; j < v.size(); j++)
				enzymes.insert(v[j]);
		}
		myfile.close();
	}
	else
		cout << "Unable to open file";

	return enzymes;
}

set<string> load_irreversible_enzyme(string &filename) {
	ifstream myfile(filename.c_str());
	set<string> enzymes;
	string line;

	if (myfile.is_open()) {
		getline(myfile, line);
		// Ignore some lines that are the beginning of the data file
		while (line.compare("-ENZIRREV"))
			getline(myfile, line);

		// Read all the remaining lines
		while (myfile.good()) {
			getline(myfile, line);
			line = trim(line);
			vector<string> v = tokenizer(line);
			for (unsigned j = 0; j < v.size(); j++)
				enzymes.insert(v[j]);
		}
		myfile.close();
	}
	else
		cout << "Unable to open file";

	return enzymes;
}

set<string> load_internal_metabolite(string &filename) {
	ifstream myfile(filename.c_str());
	set<string> enzymes;
	string line;

	if (myfile.is_open()) {
		getline(myfile, line);
		// Ignore some lines that are the beginning of the data file
		while (line.compare("-METINT"))
			getline(myfile, line);

		// Read all the remaining lines
		while (myfile.good()) {
			getline(myfile, line);
			line = trim(line);
			vector<string> v = tokenizer(line);
			for (unsigned j = 0; j < v.size(); j++)
				enzymes.insert(v[j]);
		}
		myfile.close();
	}
	else
		cout << "Unable to open file";

	return enzymes;
}

set<string> load_external_metabolite(string &filename) {
	ifstream myfile(filename.c_str());
	set<string> enzymes;
	string line;

	if (myfile.is_open()) {
		getline(myfile, line);
		// Ignore some lines that are the beginning of the data file
		while (line.compare("-METEXT"))
			getline(myfile, line);

		// Read all the remaining lines
		while (myfile.good()) {
			getline(myfile, line);
			line = trim(line);
			vector<string> v = tokenizer(line);
			for (unsigned j = 0; j < v.size(); j++)
				enzymes.insert(v[j]);
		}
		myfile.close();
	}
	else
		cout << "Unable to open file";

	return enzymes;
}
