QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = cutset

SOURCES += \
    bio.cpp \
    cutset.cpp \
    demo.cpp \
    files.cpp \
    graph.cpp \
    utils.cpp \
    mainwindow.cpp \
    about.cpp

HEADERS += \
    bio.h \
    files.h \
    graph.h \
    demo.h \
    utils.h \
    mainwindow.h \
    about.h

OTHER_FILES += \
    CutSet.pro.user \
    findcuts.cpp \
    Makefile

FORMS += \
    mainwindow.ui \
    about.ui
