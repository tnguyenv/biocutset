#ifndef MATRIX_H
#define MATRIX_H

//template <typename T>
//class Matrix
//{
//private:
//  size_t m_nRows;
//  size_t m_nCols;
//  std::vector< T > m_vect;

//public:
//  Matrix( size_t nRows=0. size_t nCols=0, const T& t= T() )
//    : m_nRows( nRows ), m_nCols (nCols), m_vect( nRows * nCols, t )
//  {
//  }

//  size_t rows() const { return m_nRows; }
//  size_t cols() const { return m_nCols; }

//  T& operator()( size_t row, size_t col )
//  {
//    assert( row < m_nRows && col < m_nCols );
//    return m_vect[ row * m_nCols + col ];
//  }

//  const T& operator()( size_t row, size_t col ) const
//  {
//    assert( row < m_nRows && col < m_nCols );
//    return m_vect[ row * m_nCols + col ];
//  }

//  T* getRow( size_t  row )
//  {
//    assert( row <= m_nRows ); // we allow one past the end
//    // this permits certain algorithms

//    return &m_vect[ row * m_nCols ];
//  }

//  const T* getRow( size_t row ) const
template <typename T>
class Matrix
{
private:
   size_t m_nRows;
   size_t m_nCols;
   std::vector< T > m_vect;

public:
   Matrix( size_t nRows=0. size_t nCols=0, const T& t= T() )
    : m_nRows( nRows ), m_nCols (nCols), m_vect( nRows * nCols, t )
   {
   }

   size_t rows() const { return m_nRows; }
   size_t cols() const { return m_nCols; }

   T& operator()( size_t row, size_t col )
   {
        assert( row < m_nRows && col < m_nCols );
        return m_vect[ row * m_nCols + col ];
   }

   const T& operator()( size_t row, size_t col ) const
   {
        assert( row < m_nRows && col < m_nCols );
        return m_vect[ row * m_nCols + col ];
   }

   T* getRow( size_t  row )
   {
         assert( row <= m_nRows ); // we allow one past the end
                                             // this permits certain algorithms

         return &m_vect[ row * m_nCols ];
   }

   const T* getRow( size_t row ) const
   {
         assert( row <= m_nRows ); // see comment above
         return &m_vect[ row * m_nCols ];
   }

   T* operator[]( size_t  row ) // same as getRow
   {
       return getRow( row );
    }

   const T* operator[]( size_t row ) const
   {
        return getRow( row );
    }

   class const_row_iterator
   {
     private:
          const Matrix * m_pMatrix;
          size_t m_nRow;

     public:
          const_row_iterator( const Matrix *pMatrix=0, size_t nRow = 0 )
             : m_pMatrix (pMatrix), m_nRow( nRow )
          {
          }

       bool operator!=( const const_row_iterator & rhs )const
       {
            return (m_row != rhs.m_row) || (m_pMatrix != rhs.m_pMatrix);
       {

       const T* operator*() const
       {
            return m_pMatrix->getRow( m_nRow );
       }

       const_row_iterator& operator++()
       {
          ++m_nRow;
          return *this;
      }

      const_row_iterator operator++( int )
      {
          const_row_iterator itCopy( *this );
          ++m_nRow;
         return itCopy;
     }
  }; // end of const_row_iterator class

   const_row_iterator begin() const
    {
       return const_row_iterator( this, 0 );
    }

    const_row_iterator end() const
    {
        return const_row_iterator( this, m_nRows );
    }
};//  {
//    assert( row <= m_nRows ); // see comment above
//    return &m_vect[ row * m_nCols ];
//  }

//  T* operator[]( size_t  row ) // same as getRow
//  {
//    return getRow( row );
//  }

//  const T* operator[]( size_t row ) const
//  {
//    return getRow( row );
//  }

//  class const_row_iterator
//  {
//  private:
//    const Matrix * m_pMatrix;
//    size_t m_nRow;

//  public:
//    const_row_iterator( const Matrix *pMatrix=0, size_t nRow = 0 )
//      : m_pMatrix (pMatrix), m_nRow( nRow )
//    {
//    }

//    bool operator!=( const const_row_iterator & rhs )const
//    {
//      return (m_row != rhs.m_row) || (m_pMatrix != rhs.m_pMatrix);
//      {

//        const T* operator*() const
//        {
//          return m_pMatrix->getRow( m_nRow );
//        }

//        const_row_iterator& operator++()
//        {
//          ++m_nRow;
//          return *this;
//        }

//        const_row_iterator operator++( int )
//        {
//          const_row_iterator itCopy( *this );
//          ++m_nRow;
//          return itCopy;
//        }
//      }; // end of const_row_iterator class

//      const_row_iterator begin() const
//      {
//        return const_row_iterator( this, 0 );
//      }

//      const_row_iterator end() const
//      {
//        return const_row_iterator( this, m_nRows );
//      }
//    };
#endif // MATRIX_H
