#include <iostream>
#include <stdlib.h>
#include <set>
#include <vector>
#include <list>
#include "stdio.h"
#include "graph.h"

using namespace std;
/*
class Graph {
private:
  bool** adjacencyMatrix;
  int vertexCount;
public:
  Graph(int vertexCount) {
    this->vertexCount = vertexCount;
    adjacencyMatrix = new bool*[vertexCount];
    for (int i = 0; i < vertexCount; i++) {
      adjacencyMatrix[i] = new bool[vertexCount];
      for (int j = 0; j < vertexCount; j++)
        adjacencyMatrix[i][j] = false;
    }
  }

  void addEdge(int i, int j) {
    if (i >= 0 && i < vertexCount && j > 0 && j < vertexCount) {
      adjacencyMatrix[i][j] = true;
      adjacencyMatrix[j][i] = true;
    }
  }

  void removeEdge(int i, int j) {
    if (i >= 0 && i < vertexCount && j > 0 && j < vertexCount) {
      adjacencyMatrix[i][j] = false;
      adjacencyMatrix[j][i] = false;
    }
  }

  bool isEdge(int i, int j) {
    if (i >= 0 && i < vertexCount && j > 0 && j < vertexCount)
      return adjacencyMatrix[i][j];
    else
      return false;
  }
  void display() {
    for (int i = 0; i < vertexCount; i++) {
      cout << adjacencyMatrix[i] <<": ";
      for (int j = 0; j < vertexCount; j++)
        if (adjacencyMatrix[i][j])
          cout << adjacencyMatrix[i][j] <<", ";
      cout <<"\n";
    }
  }
};
*/

bool checked[100];
//set<int*> vsets, combinations;
int** vsets, combinations;
set<int> layer_i, layer_i_plus;

#define NUM_EDGES 10

struct node {
  string name;
  int vertex;
  int weight;
  node(string _name, int v, int w) : name(_name), vertex(v), weight(w) { }
  node() { }
};

list<node> adjlist[NUM_EDGES];


// compprog.wordpress.com/2007/10/17/generating-combinations-1/
/* Prints out a combination like {1, 2} */
void printc(int comb[], int k) {
	printf("{");
	int i;
	for (i = 0; i < k; ++i)
		printf("%d, ", comb[i] + 1);
	printf("\b\b}\n");
}

/*
	next_comb(int comb[], int k, int n)
		Generates the next combination of n elements as k after comb

	comb => the previous combination ( use (0, 1, 2, ..., k) for first)
	k => the size of the subsets to generate
	n => the size of the original set

	Returns: 1 if a valid combination was found
		0, otherwise
*/
int next_comb(int comb[], int k, int n) {
	int i = k - 1;
	++comb[i];
	while ((i >= 0) && (comb[i] >= n - k + 1 + i)) {
		--i;
		++comb[i];
	}

	if (comb[0] > n - k) /* Combination (n-k, n-k+1, ..., n) reached */
		return 0; /* No more combinations can be generated */

	/* comb now looks like (..., x, n, n, n, ..., n).
	Turn it into (..., x, x + 1, x + 2, ...) */
	for (i = i + 1; i < k; ++i)
		comb[i] = comb[i - 1] + 1;

	return 1;
}

int create_com() {
	int n = 5; /* The size of the set; for {1, 2, 3, 4} it's 4 */
	int k = 3; /* The size of the subsets; for {1, 2}, {1, 3}, ... it's 2 */
	int comb[16]; /* comb[i] is the index of the i-th element in the
			combination */

	/* Setup comb for the initial combination */
	int i;
	for (i = 0; i < k; ++i)
		comb[i] = i;

	/* Print the first combination */
	printc(comb, k);

	/* Generate and print all the other combinations */
	while (next_comb(comb, k, n))
		printc(comb, k);

	return 0;
}

int create_combinations(int n, int k) {
  int a[k];
  int count=1;

  for(int j =0 ; j<k ; j++)
    a[j]=j;
  int i;

  do
  {
    cout.width(3);cout<<count++<<" : ";
    for(int j =0 ; j<k ; j++)
      cout<<a[j]+1<<",";
    cout<<endl;
    i=k-1;
    while (a[i]==n-k+i)
    {
      i--;
      if (i<0) break;
    }
    if (i<0) break;
    a[i]+=1;

    for(int j = i+1 ; j<k ; j++)
      a[j]=a[i]+j-i;
  }while(true);
  return 1;
}

inline int min(int x, int y) {
	return (x < y)?x:y;
}
void initialize_preflow() {
	memset(h, 0, sizeof(int)*n);
	h[s] = n;
	memset(e, 0, sizeof(int)*n);
	for (int i = n-1; i >= 0; --i)
		memset(f[i], 0. sizeof(int)*n);
	for (int i = 0; i<G[s].size(); ++i) {
		int v = G[s][i];

	}
}
void push(int u, int v) {
	int temp = min(e(u), cf[u][v]);
	f[u][v] = f[u][v] + temp;
	f[v][u] = -f[u][v];
	e[u] = e[u] - temp;
	e[v] = e[v] + temp;
	cf[u][v] = c[u][v] - f[u][v];
	cf[v][u] = c[v][u] - f[v][u];
}
