#ifndef _GRAPH_H_
#define _GRAPH_H_

#include <string>
#include <vector>
#include "bio.h"
#include "utils.h"
#include <map>

typedef std::map<std::string, float> MapType;
typedef std::map<std::string, MapType> MyMatrix;

// to use STLs despite of using std::string

/*
 *
 * This file has declarations for classes used to represent the graph
 *
 * */

//!< enum for the status_ of a node
enum Status {
	NOT_VISITED, VISITED
};

//!< utility macros
#define status(x) (x)?"VISITED":"NOT_VISITED";
/*********************************************
 This implementation applies Composite Pattern
 - Component: ElementGraph
 - Leaf: Node, Edge
 - Composite: Graph
 **********************************************/
/*
 class ElementGraph {
 public:
 ElementGraph(){} // not allowed
 ElementGraph(std::string name_) : name_(name_){}
 virtual ~ElementGraph(){}
 // traverse on the element graph
 virtual void traverse(){}
 // add an element graph into the composite
 virtual void add(ElementGraph* eg){}
 // remove an element graph out the composite
 virtual void remove(ElementGraph* eg){}
 // get the name_ of the given element graph
 virtual std::string getName() {
 return name_;
 }
 virtual void setStatus(enum Status st);
 protected:
 std::string name_;
 };*/

// forward declaration
class Node;

/** Used for
 *  holding a Edge object
 */
class Edge {
	private:
		Node* orgNode; //!< the originating node
		Node* dstNode; //!< the destination node
		int cost; //!< cost of the edge can be an arbitrary (i.e., positive or negative)
	public:
		/**@brief Constructor 1
		 * The constructor of the class.
		 * @param firstNode 	The source node of the edge.
		 * @param secondNode 	The target node of the edge.
		 * @param inCost			The cost (also called capacity or weight) of the edge.
		 */
		Edge(Node *firstNode, Node *secondNode, int inCost);
		//Edge(std::string& e);
		/**@brief Constructor 2
		 * The constructor of the class.
		 * @param orgNode 		The source node of the edge.
		 * @param dstNode			The target node of the edge.
		 * @param cost				The cost (also called capacity or weight) of the edge.
		 */
		Edge(std::string& orgNode, std::string& dstNode, int cost);
		/**
		 *
		 * @return
		 */
		~Edge();
		/**
		 * get the destination node
		 */
		Node* getDstNode();
		/**
		 * get the original node
		 */

		Node* getOrgNode();

		/**
		 * get the cost of the edge
		 */
		int getCost();

		/**
		 * Scan in graph using BFS
		 */
		void traverse();
		/**
		 * Display edge
		 */
		void display();
};

/** Used for
 *  holding a Node object
 */
class Node {
	private:
		unsigned _degree; //!< Node Degree
		float _eccentricity; //!< Eccentricity Coefficient
		float _closeness; //!< Closeness Coefficient
		std::string name_; //!< store a string value that recognizes the node
		std::vector<Node*> adjNodeList; //!< list of outgoing edges for this node
		enum Status status_; //!< used in DFS to mark the node visited
		std::string type_; //!< Reversible, Irreversible, Internal, and External
	public:
		/**
		 * A constructor without parameter, a default constructor
		 */
		Node() {
			name_ = "";
			status_ = NOT_VISITED;
			type_ = -1;
			_closeness = 0;
			_degree = 0;
			_eccentricity = 0;
		}

		/*!
		 * 	The constructor of the class.
		 * \param name_ the name of a reaction.
		 */
		Node(std::string name_);

		/**
		 * The parameters constructor
		 */
		/*
		Node(std::string name_ = "", int degree = 0, float _closeness = 0.0,
				float _eccentricity = 0.0, unsigned status_ = NOT_VISITED,
				int type_ = Graph::NMET) {

		}
		*/

		/**
		 * do not delete the adjacent nodes here...they will be deleted by graph destructor
		 */
		~Node();

		enum Status getStatus();
		//!< get the status_ of the node that value is VISITED or NOT_VISITED

		void setStatus(enum Status st);
		//!< set the status_ of the node that value is VISITED or NOT_VISITED
		//!< set the Type of the Node

		inline std::string getType() {
			return type_;
		}
		//!< Get the type_ of the node

		void setType(std::string type_) {
			this->type_ = type_;
		}
		//!< Get the type_ of the node

		bool isAdjacent(Node*); //!< Check this node is whether adjacent or not

		std::string getName(); //!< get node's name_

		void setName(std::string name); //!< set node's name_

		unsigned getDegree(); //!< get degree

		void setDegree(unsigned degree); //!< set degree

		void setEccentricity(float value); //!< set _eccentricity

		float getEccentricity(); //!< get _eccentricity

		void setCloseness(float value); //!< set _closeness

		float getCloseness(); //!< get _closeness

		void addAdjNode(Node* adj, int cost); //!< add an adjacent node

		void removeAdjNode(Node*); //!< remove an adjacent node

		std::vector<Node*>& getAdjNodeList();
		//!< get the adjacent nodes, indeed they are the adjacent edges of this node

		std::vector<Node*> getNeighbors(); //!< get neighbors

		unsigned numberOfAdjacentNodes(); //!< get the adjacent nodes: degree

		unsigned degree(); //!< get degree = the number of the adjacent nodes
		void traverse(); //!< display all adjacent nodes of this node (pointer this)
		void display(); //!< display a node
};

/** Used for
 *  holding a Graph object
 */
class Graph {
	private:
		std::vector<Node*> nodeList; //!< list of vertices: adjacency list
		std::vector<Edge*> edgeList; //!< list of edges
		bool foundCycle; //!< true if a cycle is found, false otherwise
		int desiredCycSize; //!< desired cycle size
		inline void check_esets() {

		}
	public:
		/**
		 * Default constructor
		 */
		Graph();

		/**
		 * Constructor bases on a data file and its format: edge-edge or list of reactions
		 * @param filename	The file name_
		 * @param format		The specified format
		 */
		Graph(std::string& filename, const std::string format);

		/**
		 * Constructor with the number of nodes (vertices)
		 * @param numberVertices
		 */
		Graph(unsigned numberVertices);

		/**
		 * Destructor
		 * @return
		 */
		~Graph();

		/**
		 * Get all nodes of the graph.
		 * @return
		 */
		std::vector<Node*> getNodes();

		/**
		 * Get all edges of the graph.
		 * @return
		 */
		std::vector<Edge*> getEdges();

		/**
		 * Attributes of number of nodes and edges
		 */
		unsigned numberVertices();

		/**
		 * Attributes of number of nodes and edges
		 */
		unsigned numberEdges();

		/**
		 * reset Graph to the initial status_ that all nodes haven't visited yet
		 */
		void clearVisited();

		/**
		 * add a node to Graph (pointer: this)
		 * @param nNode
		 */
		void addNode(Node *nNode);

		/**
		 * add a node to Graph via name
		 **/
		void addNode(std::string&);
		/**
		 * add a edge to Graph (pointer: this)
		 * @param nEdge
		 */
		void addEdge(Edge* nEdge);

		/**
		 * add an edge to Graph using string, format Node1 Node2 cost
		 * @param edge
		 **/
		void addEdge(std::string& edge);

		/**
		 * Adding an edge with two names of two nodes to graph
		 **/
		void addEdge(std::string&, std::string&);

		/**
		 * Adding an edge created by two nodes: node1 and node2
		 **/
		void addEdge(Node*, Node*);

		/**
		 * Remove a Node that parameter is its name_
		 * @param nodeName
		 */
		void removeNode(std::string& nodeName);

		/**
		 * Remove a nNode out the list of nodes
		 * @param nNode
		 */
		void removeNode(Node* nNode);

		/**
		 * Remove an edge out  graph
		 * @param edge: an edge object
		 * @return Nothing
		 * */
		void removeEdge(Edge* edge);

		/**
		 * Sort The List of Nodes by Eccentricities
		 * @return The Sorted Lists
		 */
		std::vector<Node*>& sortNodesByEccentricity();

		/**
		 * Sort the list of Nodes by Degrees
		 * @return The sorted list
		 */
		std::vector<Node*>& sortNodesByDegree();

		/**
		 * Sort the list of nodes by the alphabetically
		 * @return The sorted list by name_
		 */
		std::vector<Node*>& sortNodesByName();

		/**
		 * Find node by name_
		 * @param name_: a string represents the name_ of a node
		 * @return The node found
		 */
		Node* findNodeByName(std::string name_);

		/**
		 *  Find an Edge by its vertices' names: find edge by the names of its vertices
		 * @param originalVertexName: name_ of the original vertex
		 * @param targetVertexName: name_ of the target vertex
		 * @return Edge list
		 */
		Edge* findEdgeByNames(std::string originalVertexName,
		        std::string targetVertexName);
		/**
		 * traverse on graph that depends on how to scan by the edges or the nodes.
		 * We convent that dd = 0 means by the edges, otherwise by the nodes
		 * @param dd
		 */
		void traverse(int dd);
		/**
		 * implement an introductory application
		 */
		void readFlightSchedules();
		/**
		 * create a graph from a reaction object
		 */
		void createFromReaction(Reaction* reaction);
		/**
		 * create a graph from reaction string
		 */
		void createFromReaction(std::string& reaction);
		/**
		 * createMetaboliteGraphFromReaction
		 **/
		void createMetaboliteGraphFromReaction(Reaction* r);
		/**
		 * createMetaboliteGraphFromReaction
		 **/
		void createMetaboliteGraphFromReaction(std::string& reaction);

		void createReactionGraphFrom2Reactions(Reaction*, Reaction*);
		/**
		 * Getting the list of the degrees of vertices
		 *  @return A list of integers
		 */
		unsigned* degrees();
		/**
		 * Compute network centralities.
		 */
		float * bfs_eccentricity_centrality();

		/**
		 * Calculate single vertex BFS _eccentricity
		 * @param 	v The node
		 * @return 		A float value
		 */
		float single_vertex_bfs_eccentricity(Node* v);

		/**
		 * Calculate the betweeness centrality based Brandes algorithm.
		 * @param normalized
		 * @param weighted_edges
		 * @return a float value
		 */
		float brandes_betweenness_centrality(bool normalized,
		        bool weighted_edges);

		/**
		 * Determine whether or not the edge set has duplicated elements.
		 * @return A set of the indices of the duplicated edges.
		 **/
		std::set<unsigned> hasDuplicatedEdges();



		/**
		 * Determine the graph empty or not
		 */
		bool isEmpty();

		/**
		 * Set the type of the nodes. We have four  types of nodes like REV, IRREV, INT, EXT.
		 * In future work, we should attach this information to node during loading data
		 * file and creating the graph from the file.
		 * @param filename The file name_ of network.
		 */
		void classifyNodes(std::string& filename);

		/**
		 * Display the basic informations of graph including the number of vertices, the number of edges and the adjacent list.
		 */
		void display();

		/**
		 * Display edge list
		 * */
		void displayEdgeList();

		/**
		 * Display node list
		 * */
		void displayNodeList();

		/**
		 * Find adjacent nodes of a node
		 * @param node Node
		 * @return The adjacent nodes
		 */
		MapType adjacent_nodes(Node* node); //!< Get adjacent nodes

		/**
		 * Compute the distance matrix from a node to all others.
		 * @param 	node Node
		 * @return	A distance matrix
		 */
		MapType distance(Node* node); //!< Get distance matrix of a node to the others

		/**
		 * Compute the shortest path from a source node to a target node
		 * @param 	start The start node on the shortest path.
		 * @param 	end		The end node on the shortest path.
		 * @return	an unsigned value is the shortest path
		 */
		unsigned computeShortestPath(Node* start, Node* end);
		/**
		 * Generate the distance matrix
		 * @return a numerical matrix
		 */
		MyMatrix generate_distance_matrix();

		/**
		 * Generate the adjacent matrix
		 * @return a numerical matrix
		 */
		MyMatrix generate_adjacent_matrix();

		/**
		 * Extract the reaction graph from the full graph.
		 * @return A Reaction Graph
		 */
		Graph* extractReactionGraph();

		/**
		 * Extract the metabolite graph from  the full graph.
		 * @return A Metabolite Graph
		 */
		Graph* extractMetaboliteGraph();

		/**
		 * The minimal cut set algorithm
		 * @param 	s The source
		 * @param 	t The sink
		 * @return 	An integer number is the minimum cut set.
		 */
		int findMinimalCutSet(Node* s, Node* t);

		static const std::string N2N; //!< One kind of the input format. It contains a list of edges (e.g., node1 node2).
		static const std::string REC; //!< One kind of the input format. It contains a list of reactions.
		static const int NMET = 1; //!< This is the kind of metabolite node
		static const int NREC = 2; //!< This is the kind of reaction node
};
#endif
